'use strict';

module.exports = {

    'serverport': 3005,

    'styles': {
        'src' : 'src/styles/**/*.scss',
        'dest': 'dist/css'
    },

    'scripts': {
        'src' : 'src/**/*.js',
        'dest': 'dist'
    },

    'images': {
        'src' : 'src/images/**/*',
        'dest': 'dist/images'
    },

    'fonts': {
        'src' : ['src/fonts/**/*'],
        'dest': 'dist/fonts'
    },

    'views': {
        'watch': [
            'src/index.html',
            'src/**/*.html'
        ],
        'src': 'src/charts/**/*.html',
        'dest': 'src'
    },

    //'gzip': {
    //    'src': 'build/**/*.{html,xml,json,css,js,js.map}',
    //    'dest': 'build/',
    //    'options': {}
    //},

    'dist': {
        'root'  : 'dist'
    },

    'browserify': {
        'entries'   : ['./src/index.js'],
        'bundleName': 'dt-charts.js',
        'sourcemap' : true
    },

    'test': {
        'karma': 'test/karma.conf.js',
        'protractor': 'test/protractor.conf.js'
    }

};