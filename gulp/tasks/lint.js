'use strict';

var config = require('../config');
var gulp   = require('gulp');
var jshint = require('gulp-jshint');

gulp.task('lint', function() {
  return gulp.src([config.scripts.src, '!src/charts/templates.js', '!src/charts/map/featuresMock.js', '!src/charts/map/containerMock.js'])
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
});