'use strict';

var d3 = require('d3');
var _ = require('lodash');
var moment = require('moment');
var moment_es = require('moment/locale/es.js');
moment.locale('es', moment_es);

exports.inject = function(area) {
    area.directive('dtArea', exports.dtArea);
    return exports.dtArea;
};

//http://bl.ocks.org/mbostock/3883195
//https://gist.github.com/auser/6506865#file-d3-example1-js
exports.dtArea = ['$window', '$timeout', '$filter', function ($window, $timeout, $filter) {
    var linesLeftPaddingPercent = 6;
    var linesRightPaddingPercent = 3.2;

    function calculatePercent(number, percent) {
        return number * percent / 100;
    }

    function calculateWeekColumnWidth(totalWidth) {
        var currentDayColumnWidthPercent = 7.2;
        return  calculatePercent(totalWidth, currentDayColumnWidthPercent);
    }

    function increaseActual(diffPercent) {
        return diffPercent > 0;
    }

    function increasePercentLevel(diffPercent, increase) {
        var diffPercentAbs = (diffPercent < 0 ? diffPercent * -1 : diffPercent) * 100;
        if (diffPercentAbs <= 10) {
            return increase ? "verde1" : "rojo1";
        } else if (diffPercentAbs > 10 && diffPercentAbs <= 20) {
            return increase ? "verde2" : "rojo2";
        } else if (diffPercentAbs > 20 && diffPercentAbs <= 40) {
            return increase ? "verde3" : "rojo3";
        } else if (diffPercentAbs > 40 && diffPercentAbs <= 70) {
            return increase ? "verde4" : "rojo4";
        } else if (diffPercentAbs > 70) {
            return increase ? "verde5" : "rojo5";
        }
    }

    function calculateMargins(fullDiv, widthPixels, headerHeight, headerWidth, headerBottomMargin) {
        var margins;
        if (!fullDiv) {
            margins = {
                top: 20,
                right: calculatePercent(widthPixels, linesRightPaddingPercent),
                bottom: 30,
                left: calculatePercent(widthPixels, linesLeftPaddingPercent)
            };
        } else {
           margins = {
                top: headerHeight + headerBottomMargin,
                right: 0,
                bottom: headerWidth,
                left: 0
            };
        }
        return margins;
    }

    return {
        restrict: 'EA',
        templateUrl: 'area/dt-area.html',
        scope: {
            data: '=',
            lastData: '=',
            configuration: '=',
            selectedRangeStart: '=',
            selectedRangeEnd: '='
        },
        link: function (scope, ele) {
            var renderTimeout;

            var widthPixels = d3.select(ele[0].getElementsByClassName('svg-container')[0]).node().offsetWidth;
            var heightPixels = d3.select(ele[0].getElementsByClassName('svg-container')[0]).node().offsetHeight;
            var headerHeight = calculatePercent(heightPixels, 28);
            var headerBottomMargin = 30;
            var headerWidth = calculatePercent(heightPixels, 10);
            var chartMargin = {top: headerHeight, right: 0, bottom: headerWidth, left: 0};

            scope.showHeader = _.isUndefined(scope.configuration) || scope.configuration.headerSection.show;

            var $svg = ele.find("svg");
            var svg = d3.select($svg[0])
                .style('width', '100%')
                .style('height', '100%')

            var mentionsChart = svg.append("g")
                .attr("class", "mentions-chart");

            $window.onresize = function() {
                scope.$apply();
            };

            scope.$watch(function() {
                return angular.element($window)[0].innerWidth;
            }, function() {
                scope.render(scope.data, scope.lastData);
            });

            scope.$watch(function() {
                return angular.element($window)[0].innerHeight;
            }, function() {
                scope.render(scope.data, scope.lastData);
            });

            scope.$watch('data', function(newData, oldData) {
                if (newData != oldData){
                    scope.data = newData;
                    scope.render(newData, scope.lastData);
                }
            }, true);

            scope.$watch('lastData', function(newData, oldData) {
                if (newData != oldData){
                    scope.lastData = newData;
                    scope.render(scope.data, newData);
                }

            }, true);

            scope.render = function(data, lastData) {
                mentionsChart.selectAll('*').remove();


                if (!data || !lastData) return;

                if (renderTimeout) clearTimeout(renderTimeout);

                renderTimeout = $timeout(function() {

                    widthPixels = d3.select(ele[0].getElementsByClassName('svg-container')[0]).node().offsetWidth;
                    heightPixels = d3.select(ele[0].getElementsByClassName('svg-container')[0]).node().offsetHeight;
                    if (scope.showHeader) {
                        scope.avgWeek = Math.round(_.sum(_.pluck(data, 'count')) / _.size(data));
                        headerHeight = calculatePercent(heightPixels, 28);
                        headerWidth = calculatePercent(heightPixels, 10);
                    } else {
                        headerHeight = 0;
                        headerWidth = 0;
                        headerBottomMargin = 0;
                    }
                    var isFullDiv = _.isUndefined(scope.configuration) ? true : scope.configuration.fullDiv;
                    chartMargin = calculateMargins(isFullDiv, widthPixels, headerHeight, headerWidth, headerBottomMargin);
                    var width = widthPixels - chartMargin.left - chartMargin.right,
                        height = heightPixels - chartMargin.top - chartMargin.bottom;

                    var x = d3.time.scale()
                        .range([0, width]);

                    var y = d3.scale.linear()
                        .range([height, 0]);

                    x.domain(d3.extent(data, function(d) { return d.day; }));
                    y.domain([0, _.max([d3.max(data, function(d) { return d.count; }), d3.max(lastData, function(d) { return d.count; })])]);
                    var numberOfTicks = _.isUndefined(scope.configuration) ? 7 : calculateNumberOfYTicks();

                    var yAxis = d3.svg.axis()
                        .scale(y)
                        .orient("left")
                        .tickPadding(2);

                    var area = d3.svg.area()
                        .interpolate("cardinal")
                        .x(function(d) { return x(d.day); })
                        .y0(height)
                        .y1(function(d) { return y(d.count); });

                    var line = d3.svg.line()
                        .interpolate("cardinal")
                        .x(function(d) { return x(d.day); })
                        .y(function(d) { return y(d.count); });

                    //TODO: refactor magic dates, min and max date in week, refactor date parser????? in client controllor or here??
                    var minDay = data[0].day;
                    var maxDay = _.last(data).day;

                    var chart = mentionsChart.append("g")
                        .attr("class", "chart")
                        .attr("transform", "translate(" + chartMargin.left + "," + chartMargin.top + ")");

                    addLinearGradient();
                    if (_.isUndefined(scope.configuration) || scope.configuration.bottomStroke.show) {
                        addbottomStrokeChart();
                    }
                    addLineTicksYAxis();
                    addArea();
                    addYAxis();
                    addXAxis();
                    if (scope.showHeader) {
                        addHeader();
                    } else {
                        addColumnRange();
                    }
                    addSerie();

                    function addLinearGradient() {
                        var defsLinearGradient = chart.append("defs");

                        defsLinearGradient.append("linearGradient")
                            .attr("id", "day-gradient")
                            .attr("gradientUnits", "userSpaceOnUse")
                            .attr("y1", 0).attr("x1", x(minDay))
                            .attr("y2", 0).attr("x2", x(maxDay))
                            .selectAll("stop")
                            .data([
                                {offset: "0%", color: "#38DAED"},           //TODO: refactor magic colors???, from styles or constant???
                                {offset: "100%", color: "#1B72AF"}
                            ])
                            .enter().append("stop")
                            .attr("offset", function(d) { return d.offset; })
                            .attr("stop-color", function(d) { return d.color; });
                    }
                    function addLineTicksYAxis() {
                        var layer;
                        if (!_.isUndefined(scope.configuration) && scope.configuration.ticks.inFront){
                            layer = mentionsChart;
                        } else {
                            layer = chart;
                        }
                        var xPositions;
                        if (!isFullDiv) {
                            xPositions =  {
                                start: 0 + chartMargin.left,
                                end: width + chartMargin.left
                            }
                        } else {
                            xPositions = {
                                start: width * (_.isUndefined(scope.configuration) ? .1 : scope.configuration.ticks.lineTickPaddingFactor),
                                end: width - (width * (_.isUndefined(scope.configuration) ? .1 : scope.configuration.ticks.lineTickPaddingFactor))
                            };
                        }
                        mentionsChart.selectAll("line.grid")
                            .data(y.ticks(numberOfTicks))
                            .enter().append("line")
                            .attr("class", "grid")
                            .attr("x1", xPositions.start)
                            .attr("x2", xPositions.end)
                            .attr("y1", function (d) {
                                return y(d) + chartMargin.top;
                            })
                            .attr("y2", function (d) {
                                return y(d) + chartMargin.top;
                            });
                    }
                    function addbottomStrokeChart() {
                        var bottomStrokeChart = mentionsChart.append("g")
                            .attr("class", "bottom-stroke-chart")
                            .attr("transform", "translate(" + 0 + "," + (heightPixels - chartMargin.bottom) + ")");

                        bottomStrokeChart.append("linearGradient")
                            .attr("id", "bottom-stroke-gradient")
                            .attr("gradientUnits", "userSpaceOnUse")
                            .attr("y1", 0).attr("x1", x(minDay))
                            .attr("y2", 0).attr("x2", x(maxDay))
                            .selectAll("stop")
                            .data([
                                {offset: "0%", color: "#2FB7C7"},           //TODO: refactor magic colors???, from styles or constant???
                                {offset: "100%", color: "#176093"}
                            ])
                            .enter().append("stop")
                            .attr("offset", function(d) { return d.offset; })
                            .attr("stop-color", function(d) { return d.color; });

                        bottomStrokeChart.append("rect")
                            .attr("width", width)
                            .attr("height", chartMargin.bottom)
                            .attr("fill", "url(#bottom-stroke-gradient)");
                    }
                    function addArea() {
                        chart.append("path")
                            .datum(data)
                            .attr("class", "area")
                            .attr("d", area)
                            .attr("fill", "url(#day-gradient)");
                    }
                    function calculateNumberOfYTicks() {
                        var divisor = _.last(y.ticks());
                        if (divisor > 1000) {
                            return Math.round(divisor / 1000);
                        } else if (divisor > 100) {
                            return Math.round(divisor / 100);
                        } else {
                            return Math.round(divisor / 10);
                        }

                    }
                    function addYAxis() {
                        if (!isFullDiv) {
                            chart.append("g")
                                .attr("class", "y-axis no-full-div")
                                .call(yAxis
                                    .tickFormat(function (d) {
                                        return $filter('number')(d);
                                    })
                                    .ticks(numberOfTicks));
                        } else {
                            chart.append("g")
                                .attr("class", "y-axis Open-Sans-light")
                                .call(yAxis.orient("right")
                                    .ticks(7)
                                    .tickPadding(width * (_.isUndefined(scope.configuration) ? .05 : scope.configuration.ticks.tickPaddingFactor))
                                    .tickFormat(function(d) {
                                        return d === 0 ? '' : $filter('number')(d);
                                    }));
                        }
                    }
                    function addXAxis() {
                        if (!_.isUndefined(scope.configuration) && scope.configuration.showXAxis) {
                            var tickSize = 12;
                            x.nice(d3.time.hour);
                            var xAxis = d3.svg.axis()
                                .scale(x)
                                .orient("bottom")
                                .ticks(d3.time.hour, 1)
                                .tickFormat(d3.time.format("%H:%M"))
                                .tickPadding(3)
                                .innerTickSize(tickSize)
                                .outerTickSize(0);

                            chart.append("g")
                                .attr("class", "x-axis")
                                .attr("transform", "translate(0," + height + ")")
                                .call(xAxis)
                                .selectAll("line")
                                .attr("transform", "translate(0, -" + tickSize + ")");
                        }
                    }
                    function addHeader() {
                        var marginsHeader = {top: calculatePercent(heightPixels, 0.5), right: 0, bottom: 0, left: 0}; //margin top to show shadow

                        var header = mentionsChart.append("g")
                            .attr("class", "header")
                            .attr("transform", "translate(" + marginsHeader.left + "," + marginsHeader.top + ")");

                        var currentDayData = _.max(_.pluck(lastData, 'day'));   //siempre tendré los datos con una semana y un día por ambos extremos es decir 9 días

                        var startCurrentDayColumnPos = x(currentDayData);

                        var currentDayColumnWidth = calculateWeekColumnWidth(width);

                        var centeredCurrentDayColumnPos = startCurrentDayColumnPos - (currentDayColumnWidth / 2);

                        var currentDayDataColumn = [
                            {x : centeredCurrentDayColumnPos, y : 0}
                        ];

                        var columns = header.append("g")
                            .attr("class", "columns");

                        var defsColumns = columns.append("defs");

                        var filterColumns = defsColumns.append("filter")
                            .attr("id", "columns-shadow");

                        filterColumns.append("feGaussianBlur")
                            .attr("in", "SourceAlpha")
                            .attr("stdDeviation", 3)
                            .attr("result", "blur");

                        filterColumns.append("feOffset")
                            .attr("in", "blur")
                            .attr("dx", 0)
                            .attr("dy", 3)
                            .attr("result", "offsetBlur");

                        var feMergeColumn = filterColumns.append("feMerge");

                        feMergeColumn.append("feMergeNode")
                            .attr("in", "offsetBlur")
                        feMergeColumn.append("feMergeNode")
                            .attr("in", "SourceGraphic");

                        var currentDayColumnHeight = heightPixels - marginsHeader.top - marginsHeader.bottom;

                        columns.selectAll("rect")
                            .data(currentDayDataColumn)
                            .enter().append("rect")
                            .attr("class", "current-day-column")
                            .attr("width", currentDayColumnWidth)
                            .attr("height", currentDayColumnHeight)
                            .attr("stroke-width", 1)
                            .style("filter", "url(#columns-shadow)")
                            .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

                        var dayInfoBoxsHeigth = calculatePercent(currentDayColumnHeight, 21.5);
                        var dayInfoBoxsWidth = calculatePercent(currentDayColumnWidth, 74);
                        var dayInfoBoxsMarginTop = calculatePercent(currentDayColumnHeight, 7);
                        var dayInfoBoxsMarginLeft = -dayInfoBoxsWidth / 2;

                        header.selectAll(".day-info-box")
                            .data(lastData)
                            .enter().append("g")
                            .attr("class", "day-info-boxs")
                            .attr("transform", function(d) {
                                return "translate(" + x(d.day) + "," + 0 + ")";
                            })
                            .append("foreignObject")
                            .attr("x", dayInfoBoxsMarginLeft)
                            .attr("y", dayInfoBoxsMarginTop)
                            .attr({width: dayInfoBoxsWidth, height: dayInfoBoxsHeigth})
                            .append("xhtml:body")
                            .style("margin", "auto")
                            .style("background-color", "transparent")
                            .style("line-height", "1.5")
                            .append("xhtml:div")
                            .style({
                                width: dayInfoBoxsWidth + 'px',
                                height: dayInfoBoxsHeigth + 'px',
                                "font-size": "11px",
                                "background-color": "transparent",
                                "text-align": "center"
                            })
                            .html(function (d, i) {
                                moment.locale('es');
                                var actualData = d.count, referenceData = (_.first(_.where(data, { 'day': d.day}))).count;        //Todo refactor: same length y both arrays (avg and mentions)
                                var diffPercent = referenceData !== 0 ? (actualData - referenceData) / referenceData : 1;
                                var increase = increaseActual(diffPercent);
                                return "<div> <div style='height:" + calculatePercent(dayInfoBoxsHeigth , 25) + "px; margin-bottom: 5px;'><img style='width: 30px;' src='images/i_" + increasePercentLevel(diffPercent, increase) + ".svg'> </div>" +
                                    "<p style='margin: auto;'><strong>" + Math.round(diffPercent * 100) + "%</strong></p> <p style='margin: auto;'>" + moment(d.day).format("dddd").toUpperCase() +
                                    "</p> <p style='margin: auto;'><strong>" + (i === lastData.length - 1 ? "Hoy " : "") + moment(d.day).format("D") + " &middot " + moment(d.day).format("MM") + "</strong></p></div>";
                            });

                    }
                    function addColumnRange() {
                        var columns = mentionsChart.append("g")
                            .attr("class", "columns");

                        var defsColumns = columns.append("defs");

                        var filterColumns = defsColumns.append("filter")
                            .attr("id", "columns-selected-shadow");

                        filterColumns.append("feGaussianBlur")
                            .attr("in", "SourceAlpha")
                            .attr("stdDeviation", 3)
                            .attr("result", "blur");

                        filterColumns.append("feOffset")
                            .attr("in", "blur")
                            .attr("dx", 0)
                            .attr("dy", 2)
                            .attr("result", "offsetBlur");

                        var feMergeColumn = filterColumns.append("feMerge");

                        feMergeColumn.append("feMergeNode")
                            .attr("in", "offsetBlur")
                        feMergeColumn.append("feMergeNode")
                            .attr("in", "SourceGraphic");

                        var selectedRangeColumnData = [
                            {x : x(scope.selectedRangeStart) + chartMargin.left, y : 3}
                        ];
                        var selectedRangeColumnHeight = heightPixels - 5;
                        var selectedRangeColumnWidth = x(scope.selectedRangeEnd) - x(scope.selectedRangeStart);
                        columns.selectAll("rect")
                            .data(selectedRangeColumnData)
                            .enter().append("rect")
                            .attr("class", "current-day-column")
                            .attr("width", selectedRangeColumnWidth)
                            .attr("height", selectedRangeColumnHeight)
                            .attr("stroke-width", 1)
                            .style("filter", "url(#columns-selected-shadow)")
                            .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
                    }
                    function addSerie() {
                        var serie = mentionsChart.append("g")
                            .attr("class", "current-mentions")
                            .attr("transform", "translate(" + chartMargin.left + "," + chartMargin.top + ")");

                        //http://bl.ocks.org/cpbotha/5200394
                        var defs = serie.append("defs");

                        var filter = defs.append("filter")
                            .attr("id", "line-shadow")
                            .attr("height", "220%");

                        filter.append("feGaussianBlur")
                            .attr("in", "SourceAlpha")
                            .attr("stdDeviation", 9)
                            .attr("result", "blur");

                        filter.append("feOffset")
                            .attr("in", "blur")
                            .attr("dx", 8)
                            .attr("dy", 20)
                            .attr("result", "offsetBlur");

                        var feMerge = filter.append("feMerge");

                        feMerge.append("feMergeNode")
                            .attr("in", "offsetBlur")
                        feMerge.append("feMergeNode")
                            .attr("in", "SourceGraphic");

                        serie.append("path")
                            .datum(lastData)
                            .attr("class", "last-week")
                            .attr("d", line)
                            .style("stroke", "#442C56")
                            .style("stroke-width", "4")
                            .style("fill", "none")
                            .style("filter", "url(#line-shadow)");

                        //https://gist.github.com/trtg/3922684
                        d3.selection.prototype.moveToFront = function() {
                            return this.each(function(){
                                this.parentNode.appendChild(this);
                            });
                        };

                        var defsCircle = serie.append("defs");

                        var filterCircle = defsCircle.append("filter")
                            .attr("id", "point-shadow")
                            .attr("height", "180%");

                        filterCircle.append("feGaussianBlur")
                            .attr("in", "SourceAlpha")
                            .attr("stdDeviation", 6)
                            .attr("result", "blur");

                        filterCircle.append("feOffset")
                            .attr("in", "blur")
                            .attr("dx", 5)
                            .attr("dy", 5)
                            .attr("result", "offsetBlur");

                        var feMergeCircle = filterCircle.append("feMerge");

                        feMergeCircle.append("feMergeNode")
                            .attr("in", "offsetBlur")
                        feMergeCircle.append("feMergeNode")
                            .attr("in", "SourceGraphic");

                        var tooltip;
                        if (_.isUndefined(scope.configuration)) {
                            tooltip = d3.select("body")
                                .append("div")
                                .style("color", "#ffffff")
                                .style("font-size", "10px")
                                .style("text-align", "center")
                                .append("span")
                                .style("padding", "12px")
                                .style("position", "absolute")
                                .style("z-index", "10")
                                .style("background-color", "rgb(231, 0, 135)")
                                .style("width", "auto")
                                .style("height", "auto")
                                .style("visibility", "hidden");
                        } else {
                            tooltip = d3.select('.svg-container').append("div")
                                .attr("class", 'volume-detail-tooltip')
                                .style('display', "none");
                        }

                        var pointsInRangeColData = _.filter(lastData, function(pointData) {
                            return pointData.day >= scope.selectedRangeStart && pointData.day <= scope.selectedRangeEnd;
                        });
                        var maxPointCount = _.max(_.pluck(pointsInRangeColData, 'count'));
                        serie.selectAll(".dot")
                            .data(lastData)
                            .enter().append("circle")
                            .attr("class", "dot")
                            .attr("cx", line.x())
                            .attr("cy", line.y())
                            .attr("r", 5)
                            .style("stroke", "white")
                            .style("stroke-width", 8)
                            .style("fill", "#442C56")
                            .style("display", function (d) {
                                if (_.isUndefined(scope.configuration)) {
                                    return "block";
                                } else {

                                    return scope.configuration.serie.pointCircles.showOnlyMax
                                        && d.count === maxPointCount ? "block" : "none";
                                }
                            })
                            .on("mouseover", function(d) {
                                if (_.isUndefined(scope.configuration)) {
                                    tooltip.html("<span><strong>" + ($filter('number')(d.count)) + "</strong> Mensajes</span>");
                                    return tooltip.style("visibility", "visible");
                                }
                            })
                            .on("mousemove", function(d){
                                if (_.isUndefined(scope.configuration)) {
                                    return tooltip.style("top", (d3.event.pageY - 20) + "px").style("left", (d3.event.pageX + 20) + "px");
                                } else {
                                    updateToolTip(d);
                                }
                            })
                            .on("mouseout", function (d){
                                if (_.isUndefined(scope.configuration)) {
                                    return tooltip.style("visibility", "hidden");
                                } else {
                                    tooltip.classed("hidden", true);
                                }
                            })
                        //.style("filter", "url(#point-shadow)");

                        function updateToolTip(d) {
                            d3.event.preventDefault();
                            var mouse = d3.mouse(svg.node()).map( function(d) { return parseInt(d); } );
                            //FIXME: refactor to directive
                            tooltip
                                .classed("hidden", false)
                                .attr("style", "left:"+(mouse[0] - 55)+"px;top:"+ (mouse[1]) +"px")
                                .html("<p class='Open-Sans-light' style='width: 54px; height: 44px; margin: 0px; font-size: 12px;'> <strong class='Open-Sans-extra-bold' style='font-size: 18px;'>" + $filter('number')((d.count).toFixed(1)) + "</strong> Mensajes</p>");        //Get CCAA by norm_code
                        }

                        var sel = d3.selectAll(".dot");
                        sel.moveToFront();
                    }
                }, 0);
            };
        }
    };
}];