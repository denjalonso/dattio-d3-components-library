'use strict';

var d3 = require('d3');
var _ = require('lodash');

exports.inject = function(horizontalBars) {
    horizontalBars.directive('dtHorizontalBars', exports.dtHorizontalBars);
    return exports.dtHorizontalBars;
};

var dtHorizontalBars = function ($window, $timeout, $filter) {
    function calculatePercent(number, percent) {
        return number * percent / 100;
    }
    return {
        restrict: 'EA',
        replace: true,
        templateUrl: 'horizontal-bars/dt-horizontal-bars.html',
        scope: {
            items: '=',
            configuration: '='
        },
        link: {
            pre: function (scope, ele, attrs) {
                //http://www.ng-newsletter.com/posts/d3-on-angular.html
                var renderTimeout;

                //TODO: to constant
                var defaultBarHeight = 32;
                var defaultBarMargin = 32;
                var defaultBarPadding = 5;
                var linesBottomPaddingPercent = scope.configuration.linesBottomPaddingPercent;
                var barsTopPaddingPercent = scope.configuration.barsTopPaddingPercent;
                var precisionPixel = 1;
                var barsSectionHeightPercent = scope.configuration.barsSectionHeightPercent;
                var minTextWidth = 13;

                var margin = parseInt(attrs.margin) || defaultBarMargin,
                    barHeight = parseInt(attrs.barHeight) || defaultBarHeight,
                    barPadding = parseInt(attrs.barPadding) || defaultBarPadding;

                var widthPixels = d3.select(ele[0].getElementsByClassName('svg-container-horizontal-bars')[0]).node().offsetWidth;
                var heightPixels = d3.select(ele[0].getElementsByClassName('svg-container-horizontal-bars')[0]).node().offsetHeight;
                var chartMargin = {top: 0, right: 15, bottom: 0, left: scope.configuration.barsLeftPaddingForLegend};

                scope.barsAdded = false;
                scope.barsSectionTopPadding = 0;

                var svg = d3.select(ele.find("svg")[0]);

                var horizontalChart = svg.append("g")
                    .attr("class", "horizontal-chart");

                var chart = horizontalChart.append("g")
                    .attr("class", "chart")
                    .attr("transform", "translate(" + chartMargin.left + "," + chartMargin.top + ")");

                var horizontalBarTooltip = d3.select('.svg-container-horizontal-bars').append("div")
                    .attr("class", "horizontal-bar-tooltip")
                    .style('display', "none");

                $window.onresize = function () {
                    scope.$apply();
                };

                scope.$watch(function () {
                    return angular.element($window)[0].innerWidth || angular.element($window)[0].innerHeight;
                }, function () {
                    $timeout(function () {
                        scope.render();
                    }, 750);    //Same as reset transition
                });

                scope.$watch('items', function(newVals, oldVals) {
                    if (newVals !== oldVals) {
                        $timeout(function () {
                            scope.render();
                        }, 750);    //Same as reset transition
                    }
                }, false);

                scope.render = function () {
                    horizontalChart.selectAll("*").remove();

                    if (renderTimeout) clearTimeout(renderTimeout);

                    renderTimeout = $timeout(function () {

                        widthPixels = d3.select(ele[0].getElementsByClassName('svg-container-horizontal-bars')[0]).node().offsetWidth;
                        heightPixels = d3.select(ele[0].getElementsByClassName('svg-container-horizontal-bars')[0]).node().offsetHeight;
                        chartMargin = {top: 0, right: 15, bottom: calculatePercent(heightPixels, linesBottomPaddingPercent) + precisionPixel, left: scope.configuration.barsLeftPaddingForLegend};
                        var width = widthPixels - chartMargin.left - chartMargin.right,
                            height = heightPixels - chartMargin.top - chartMargin.bottom;

                        var barsSectionHeight = calculatePercent(height, barsSectionHeightPercent);
                        var numberOfBars = scope.items.length;
                        var calculatedBarHeight = ((barsSectionHeight / numberOfBars).toFixed(2) || barHeight) - barPadding;
                        scope.calculatedBarHeight = calculatedBarHeight;
                        var barsSectionTopPadding = calculatePercent(heightPixels, barsTopPaddingPercent);
                        scope.barsSectionTopPadding = barsSectionTopPadding;
                        scope.barPadding = barPadding;
                        scope.sortedItems = _.sortBy(scope.items, 'volume').reverse();

                        var color = d3.scale.category20(),
                            minVolume = 0,
                            maxVolume = d3.max(scope.items, function(d) {
                                return d.volume;
                            }),
                            xScale = d3.scale.linear()
                                .domain([0, maxVolume])
                                .range([0, width]);

                        var xAxis = d3.svg.axis()
                            .scale(xScale)
                            .tickFormat(function(d) {
                                return $filter('number')(d)
                            });


                        chart = horizontalChart.append("g")
                            .attr("class", "chart")
                            .attr("transform", "translate(" + chartMargin.left + "," + chartMargin.top + ")");


                        scope.drawedData =_.filter(scope.sortedItems, function (item) {
                            return item.volume !== 0;
                        });

                        addXScale();
                        addLineTicks();
                        addBarsGradient();
                        addBars();
                        addBarsLabels();

                        function addBars() {
                            //http://bl.ocks.org/mbostock/3468167
                            chart.selectAll("path.bars")
                                .data(scope.drawedData)
                                .enter()
                                .append("path")
                                .attr("class", "bars")
                                .attr("d", function(d, i) {
                                    var x = 0, y = barsSectionTopPadding + i * (calculatedBarHeight + barPadding), radius = 10;
                                    return rightRoundedRect(x, y, 10, calculatedBarHeight, radius);
                                })
                                .attr("fill", "url(#bars-gradient)")
                                .attr('cursor','pointer')
                                .on("mouseover",function (d, i) {
                                    d3.select(this).style('fill-opacity', .2);
                                    if (xScale(d.volume) < (minTextWidth + 10)) { //10 = barRightPaddig
                                        d3.select("text.label.label-" + d.brand).transition()
                                            .duration(250)
                                            .text(d.volume)
                                            .attr("x", function(d) {
                                                var barRightPaddig = 10;
                                                return xScale(maxVolume / 5) - barRightPaddig;
                                            });

                                        d3.select(this).transition()
                                            .duration(250)
                                            .attr("d", function(d) {
                                                var i = _.indexOf(_.pluck(scope.sortedItems, 'brand'), d.brand);
                                                var x = 0, y = barsSectionTopPadding + i * (calculatedBarHeight + barPadding);
                                                var radius;
                                                    radius = 10;
                                                return rightRoundedRect(x, y, xScale(maxVolume / 5), calculatedBarHeight, radius);
                                            });

                                    }
                                })
                                .on("mouseout", function(d, i) {
                                    d3.selectAll('path.bars').style('fill-opacity', function () {
                                        return 1;
                                    });
                                    horizontalBarTooltip.classed("hidden", true);
                                    if (xScale(d.volume) < (minTextWidth + 10)) { //10 = barRightPaddig
                                        d3.select("text.label.label-" + d.brand).transition()
                                            .duration(250)
                                            .text("...")
                                            .attr("x", function(d) {
                                                var barRightPaddig = 10;
                                                return xScale(d.volume) - barRightPaddig;
                                            });

                                        d3.select(this).transition()
                                            .duration(250)
                                            .attr("d", function(d) {
                                                var i = _.indexOf(_.pluck(scope.sortedItems, 'brand'), d.brand);
                                                var x = 0, y = barsSectionTopPadding + i * (calculatedBarHeight + barPadding);
                                                var radius;
                                                    radius = 10;
                                                var minRightWidth = radius;
                                                return rightRoundedRect(x, y, xScale(d.volume) < minRightWidth ? minRightWidth : xScale(d.volume), calculatedBarHeight, radius);
                                            });
                                    }
                                })
                                .on("mousemove", function(d, i) {
                                    updateToolTip(d);
                                })
                                .transition()
                                .duration(750)
                                .attr("d", function(d, i) {
                                    var x = 0, y = barsSectionTopPadding + i * (calculatedBarHeight + barPadding);
                                    var radius = 10;
                                    var minRightWidth = radius;
                                    return rightRoundedRect(x, y, xScale(d.volume) < minRightWidth ? minRightWidth : xScale(d.volume), calculatedBarHeight, radius);
                                });
                            scope.barsAdded = true;
                        }
                        function rightRoundedRect(x, y, width, height, radius) {
                            return "M" + x + "," + y
                                + "h" + (width - radius)
                                + "a" + radius + "," + radius + " 0 0 1 " + radius + "," + radius
                                + "v" + (height - 2 * radius)
                                + "a" + radius + "," + radius + " 0 0 1 " + -radius + "," + radius
                                + "h" + (radius - width)
                                + "z";
                        }
                        function addBarsLabels() {
                            //http://alignedleft.com/tutorials/d3/making-a-bar-chart
                            chart.selectAll("text.label")
                                .data(scope.drawedData)
                                .enter()
                                .append("text")
                                .attr("pointer-events", "none")
                                .attr("class", function (d) {
                                    return "label label-" + d.brand;
                                })
                                .attr("text-anchor", "end")
                                .text(function (d) {
                                    //TODO: dynamic minTextWidth, depend of number of digits
                                    return xScale(d.volume) < minTextWidth + 10 ? "..." : $filter('number')(d.volume); //10 = barRightPaddig
                                })
                                .attr("x", function(d) {
                                    var barRightPaddig = 10;
                                    return xScale(d.volume) - barRightPaddig;
                                })
                                .attr("y", function(d, i) {
                                    var barsSectionHeight = calculatePercent(height, barsSectionHeightPercent);
                                    var numberOfBars = scope.items.length;
                                    var barsSectionTopPadding = calculatePercent(heightPixels, barsTopPaddingPercent);
                                    var calculatedBarHeight = ((barsSectionHeight / numberOfBars).toFixed(2) || barHeight) - barPadding;
                                    var halfTextHeight = 4;
                                    return barsSectionTopPadding + (calculatedBarHeight / 2) + halfTextHeight + i * (calculatedBarHeight + barPadding);
                                })
                                .attr("font-family", "sans-serif")
                                .attr("font-size", "11px")
                                .attr("fill", "white");
                        }
                        function addBarsGradient() {
                            chart.append("linearGradient")
                                .attr("id", "bars-gradient")
                                .attr("gradientUnits", "userSpaceOnUse")
                                .attr("y1", 0).attr("x1", xScale(minVolume))
                                .attr("y2", 0).attr("x2", xScale(maxVolume))
                                .selectAll("stop")
                                .data([
                                    {offset: "0%", color: "#86d7e9"},           //TODO: refactor magic colors???, from styles or constant???
                                    {offset: "100%", color: "#4571ab"}
                                ])
                                .enter().append("stop")
                                .attr("offset", function(d) { return d.offset; })
                                .attr("stop-color", function(d) { return d.color; });
                        }
                        function addLineTicks() {
                            chart.selectAll("line.grid")
                                .data(xScale.ticks(5))
                                .enter().append("line")
                                .attr("class", function (d, i) {
                                    return "grid " + (i % 2 === 0 ? "par-lines" : "impar-lines");
                                })
                                .attr("y1", 0)
                                .attr("y2", height)
                                .attr("x1", xScale)
                                .attr("x2", xScale);
                        }
                        function addXScale () {
                            chart.append("g")
                                .attr("class", "x-axis")
                                .attr("transform", "translate(0," + height + ")")
                                .call(xAxis.ticks(5))
                        }
                        function updateToolTip(d) {
                            d3.event.preventDefault();
                            var mouse = d3.mouse(svg.node()).map( function(d) { return parseInt(d); } );
                            //FIXME: refactor to directive
                            horizontalBarTooltip
                                .classed("hidden", false)
                                .attr("style", "left:"+(mouse[0]- 55)+"px;top:"+ (mouse[1]) +"px")
                                .html("<p class='Open-Sans-light' style='width: 54px; height: 44px; margin: 0px; font-size: 12px;'> <strong class='Open-Sans-extra-bold' style='font-size: 18px;'>" + $filter('number')((d.volume * 100 / _.sum(_.pluck(scope.items, 'volume'))).toFixed(1)) + "%</strong> del total</p>");        //Get CCAA by norm_code
                        }
                    }, 0);
                }
            }
        }
    };
}
exports.dtHorizontalBars = ['$window', '$timeout', '$filter', dtHorizontalBars];