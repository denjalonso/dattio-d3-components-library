'use strict';

var horizontalBars = module.exports = angular.module('dtcharts.charts.horizontalBars', []);

require('./dtHorizontalBars').inject(horizontalBars);