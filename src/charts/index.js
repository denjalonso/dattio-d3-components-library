'use strict';

var mapModule = require('./map');
var pieModule = require('./pie');
var horizontalBarsModule = require('./horizontal-bars');
var multilineSeriesModule = require('./multi-line-series');
var percentHorizontalBarsModule = require('./percent-horizontal-bars');
var verticalBarsModule = require('./vertical-bars');
var multiLineAxisModule = require('./multi-line-multi-axis');
var verticalBarMultiDimensionModule = require('./vertical-bars-multi-dimensions');
var timelines = require('./timelines');
var progressBar = require("./progress-bar");
var treemap = require("./treemap");

mapModule.$inject = ['$window', '$timeout', 'componentsBBoxMargin'];
pieModule.$inject = ['$window', '$timeout', 'componentsBBoxMargin'];
horizontalBarsModule.$inject = ['$window', '$timeout'];
multilineSeriesModule.$inject = ['$window', '$timeout'];
percentHorizontalBarsModule.$inject = ['$window', '$timeout'];
verticalBarsModule.$inject = ['$window', '$timeout'];
multiLineAxisModule.$inject = ['$window', '$timeout'];
verticalBarMultiDimensionModule.$inject = ['$window', '$timeout'];
timelines.$inject = ['$window', '$timeout'];
progressBar.$inject = ['$window', '$timeout'];
treemap.$inject = ['$window', '$timeout', '$filter'];

module.exports = angular.module('dtcharts.charts', [
    mapModule.name,
    pieModule.name,
    horizontalBarsModule.name,
    multilineSeriesModule.name,
    percentHorizontalBarsModule.name,
    verticalBarsModule.name,
    multiLineAxisModule.name,
    verticalBarMultiDimensionModule.name,
    require('./area').name,
    timelines.name,
    progressBar.name,
    treemap.name
]);
