"use strict";

exports.inject = function(map) {
    map.factory('mapConfiguration', exports.mapConfiguration)
    return exports.mapConfiguration;
};

exports.mapConfiguration = function () {
    return {
        endpoint: {
            shape: "",
            container: "",
            ancestors: ""
        },
        granularityOrder: [],
        cache: {
            db: "shapes",
            store: "shapes",
            version: "2"
        },
        token: ""
    };
};