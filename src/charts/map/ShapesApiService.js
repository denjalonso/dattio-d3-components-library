"use strict";

var _ = require('lodash');
var $ = require('jquery');

exports.inject = function(map) {
    map.factory('shapesApiService', exports.shapesApiService);
    return exports.shapesApiService;
};

exports.shapesApiService = ["mapConfiguration", "topoGeoJSONUtils", "$http", function (mapConfiguration, topoGeoJSONUtils) {

    var prepareRequestParams = function (normCodes, url) {
        //TODO: refactor normCode to normCodes
        var data = {normCode: _.compact(normCodes)};

        var params = {
            url: url,
            dataType: "json"
        };

        //TODO: refactor data.normCode to data.normCodes
        if (data.normCode.length > 100) {
            // Post
            params.type = "POST";
            params.contentType = "application/json";
            params.data = JSON.stringify(data);
            params.headers = {
                'X-Auth-Token': mapConfiguration.token
            }
        } else {
            // Get
            params.type = "GET";
            params.data = data;
            params.traditional = true;
            params.headers = {
                'X-Auth-Token': mapConfiguration.token
            }
        }

        return params;
    };

    var mapShapesApiService = {
        getShapes: function (normCodes) {
            var url = mapConfiguration.endpoint.shape;
            var params = prepareRequestParams(normCodes, url);
            return $.ajax(params).then(function (response) {
                //return topoGeoJSONUtils.geoJsonToShapeList(response);
                return response;
            });
        },

        getContainer: function (normCodes) {
            var url = mapConfiguration.endpoint.container;
            var params = prepareRequestParams(normCodes, url);
            return $.ajax(params);
        },

        getChildsShapesCollection : function (childsNormCodes, collectionNormCode) {
            var url = mapConfiguration.endpoint.shape;
            var params = prepareRequestParams(childsNormCodes, url);
            return $.ajax(params).then(function (response) {    //Data response come as topoJson, this depend of the api
                return topoGeoJSONUtils.topoJsonToShapeList(response, collectionNormCode);
            });
        }
    };

    return mapShapesApiService;
}
];