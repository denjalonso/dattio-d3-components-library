"use strict";

var _ = require('lodash');

exports.inject = function(map) {
    map.factory('shapesCanaryTranslateService', exports.factory);
    return exports.factory;
};

//Any extra dependencies can just be passed in
exports.factory = function (canaryFeaturesData, canaryTranslateCoordinates) {

    function checkIfCanaryIsNotContainer(container) {
        return !_.isEqual(container.id, canaryFeaturesData.canariasId);
    };

    function canaryFeaturesAreLoaded(features) {
        return _.some(_.pluck(features, 'id'), function (featureId) {
            return _.contains(canaryFeaturesData, featureId);
        });
    };

    function changeCoordinate(val) {
        return [val[0] + canaryTranslateCoordinates.translateCoordinateX, val[1] + canaryTranslateCoordinates.translateCoordinateY];
    };

    function traverseChangingCoordinates(coordinates) {
        var nCoordinates = coordinates.length, result = [];
        while (nCoordinates--) {
            if (typeof coordinates[nCoordinates] !== 'number' && !(coordinates[nCoordinates] instanceof Array)) continue;
            if (coordinates[nCoordinates] instanceof Array) {
                result.unshift(traverseChangingCoordinates(coordinates[nCoordinates]));
                continue;
            }
            result = changeCoordinate(coordinates);
        }
        return result;
    };

    var ShapesCanaryTranslateService = {
        changeCanaryCoordinates: function (features) {
            return _.each(features, function (feature) {
                if (_.contains(canaryFeaturesData, feature.id)) {
                    feature.geometry.coordinates = traverseChangingCoordinates(feature.geometry.coordinates);
                }
            })
        }
    };

    return ShapesCanaryTranslateService;
};


