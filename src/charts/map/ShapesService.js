"use strict";

var _ = require('lodash');
var $ = require('jquery');

exports.inject = function(map) {
    map.factory('shapesService', exports.shapesService);
    return exports.shapesService;
};

exports.shapesService = ["shapesApiService", "shapesStoreService", "topoGeoJSONUtils",
    function (shapesApiService, shapesStoreService, topoGeoJSONUtils) {

        var mixDbAndApiShapes = function (dbShapes, apiShapes) {
            var allShapes = [];
            for (var i = 0; i < dbShapes.length; i++) {
                if (_.isUndefined(dbShapes[i])) {
                    allShapes[i] = apiShapes.splice(0, 1)[0];
                } else {
                    allShapes[i] = dbShapes[i];
                }
            }
            return allShapes;
        };

        var shapesService = {
            fetchShapesCollection: function (childsNormCodes, collectionNormCode) {
                var validChildsNormCodes = _.compact(childsNormCodes);
                if (validChildsNormCodes.length === 0 || _.isUndefined(collectionNormCode)) {
                    return new $.Deferred().resolveWith(null, [[]]).promise();
                }
                var result = shapesStoreService.get(collectionNormCode)
                    .then(function (dbShapes) {
                        var result;
                        var dbNormCodes = _.chain(dbShapes).compact().pluck("normCode").value();
                        if (!_.includes(dbNormCodes, collectionNormCode)) {
                            result = new $.Deferred();
                            shapesApiService.getChildsShapesCollection(childsNormCodes, collectionNormCode)
                                .done(function (apiShapes) {
                                    shapesStoreService.save(apiShapes, collectionNormCode);
                                    var shapeList = mixDbAndApiShapes(dbShapes, [apiShapes]);
                                    var topoJSON = topoGeoJSONUtils.shapeListToTopoJson(shapeList);
                                    result.resolveWith(null, [topoJSON]);
                                }).fail(result.reject);
                        } else {
                            result = topoGeoJSONUtils.shapeListToTopoJson(dbShapes);
                        }
                        return result;
                    });
                return result;
            },

            fetchShapes: function (normCodes) {
                var result = shapesStoreService.get(normCodes)
                    .then(function (dbShapes) {
                        var result;
                        var dbNormCodes = _.chain(dbShapes).compact().pluck("normCode").value();
                        var notDbNormCodes = _.difference(_.compact(normCodes), dbNormCodes);
                        if (notDbNormCodes.length !== 0) {
                            result = new $.Deferred();
                            shapesApiService.getShapes(notDbNormCodes)
                                .done(function (apiShapes) {
                                    shapesStoreService.save(apiShapes);
                                    var shapeList = mixDbAndApiShapes(dbShapes, apiShapes);
                                    var geoJSON = topoGeoJSONUtils.shapeListToGeoJson(shapeList);
                                    result.resolveWith(null, [geoJSON.features]);
                                }).fail(result.reject);
                        } else {
                            result = topoGeoJSONUtils.shapeListToGeoJson(dbShapes).features;
                        }
                        return result;
                    });
                return result;
            },

            fetchContainer: function (normCodes) {
                var validNormCodes = _.compact(normCodes);
                if (validNormCodes.length === 0) {
                    return new $.Deferred().resolveWith(null, [[]]).promise();
                }

                var result = shapesApiService.getContainer(normCodes)
                    .then(function (response) {
                        var containerNormCode = response.normCode;
                        return shapesService.fetchShapes([containerNormCode]);
                    }).then(function (shapes) {
                        if (shapes) {
                            return shapes[0];
                        }
                    });
                return result;
            }
        };

        return shapesService;
    }
];


