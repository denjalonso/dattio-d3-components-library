"use strict";

var _ = require('lodash');
var $ = require('jquery');

exports.inject = function(map) {
    map.factory('shapesStoreService', exports.shapesStoreService);
    return exports.shapesStoreService;
};



var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;

var module = angular.module("dtcharts.charts.map");

module.factory("shapesIndexedDbStore", [
    "mapConfiguration",
    function (mapConfiguration) {
        var DB = mapConfiguration.cache.db;
        var STORE = mapConfiguration.cache.store;

        if (!DB) {
            throw new Error("mapConfiguration.cache.db not configured");
        }
        if (!STORE) {
            throw new Error("mapConfiguration.cache.store not configured");
        }

        var db;

        var dbOpen = function () {
            var result = new $.Deferred();
            if (db) {
                result.resolveWith(null, [db]);
            } else {
                var version = parseInt(mapConfiguration.cache.version, 10) || 1;
                var request = indexedDB.open(DB, version);

                request.onupgradeneeded = function (e) {
                    var db = e.target.result;

                    if (db.objectStoreNames.contains(STORE)) {
                        db.deleteObjectStore(STORE);
                    }

                    db.createObjectStore(STORE, {keyPath: "normCode", autoIncrement: false});
                };
                request.onerror = function () {
                    result.reject();
                };
                request.onsuccess = function (e) {
                    db = e.target.result;
                    result.resolveWith(null, [db]);
                };
            }
            return result.promise();
        };

        var dbExecute = function (callback) {
            var deferred = new $.Deferred();
            var open = dbOpen();
            open.done(function (db) {
                callback(db, deferred);
            });
            open.fail(function () {
                console.log("Error opening db");
            });
            return deferred.promise();
        };

        var dbTransactionError = function () {
            console.log("Se produjo un error en la transacción");
        };

        var mapShapesIndexedDbStore = {

            save: function (shapes) {
                shapes = _.isArray(shapes) ? shapes : [shapes];
                var promise = dbExecute(function (db, deferred) {
                    var transaction = db.transaction([STORE], "readwrite");
                    transaction.onerror = dbTransactionError;

                    var objectStore = transaction.objectStore(STORE);
                    _.each(shapes, function (shape) {
                        objectStore.put(shape);
                    });

                    transaction.oncomplete = function (e) {
                        deferred.resolve();
                    };

                    transaction.onerror = function (e) {
                        deferred.reject();
                    };
                });
                return promise;
            },

            get: function (normCodes) {
                var promise = dbExecute(function (db, deferred) {
                    var transaction = db.transaction([STORE]);
                    transaction.onerror = dbTransactionError;

                    var objectStore = transaction.objectStore(STORE);

                    var result;
                    if (_.isArray(normCodes)) {
                        result = [];
                        _.each(normCodes, function (normCode) {
                            if (normCode) {
                                objectStore.get(normCode).onsuccess = function (e) {
                                    result.push(e.target.result);
                                };
                            } else {
                                result.push(undefined);
                            }
                        });
                    } else {
                        if (normCodes) {
                            objectStore.get(normCodes).onsuccess = function (e) {
                                result = [e.target.result];
                            };
                        }
                    }

                    transaction.oncomplete = function () {
                        deferred.resolveWith(null, [result]);
                    };
                    transaction.onerror = function () {
                        var error = "Error";
                        deferred.rejectWith(null, [error]);
                    };
                });
                return promise;
            },

            clear: function () {
                var promise = dbExecute(function (db, deferred) {
                    var transaction = db.transaction([STORE], "readwrite");
                    transaction.onerror = dbTransactionError;

                    var objectStore = transaction.objectStore(STORE);
                    var req = objectStore.clear();

                    req.onsuccess = function () {
                        deferred.resolve();
                    };

                    req.onerror = function (e) {
                        var error = "Error";
                        deferred.rejectWith(null, [error]);
                    };
                });
                return promise;
            }
        };

        return mapShapesIndexedDbStore;
    }
]);

module.factory("shapesHashStore", function () {
    var cache = {};

    var getShape = function (normCode) {
        return cache[normCode];
    };

    var saveShape = function (shape) {
        cache[shape.normCode] = shape;
    };

    var emptyResolvedPromise = function () {
        var result = new $.Deferred();
        result.resolve();
        return result.promise();
    };

    var mapShapesHashStore = {

        get: function (normCodes) {
            var shapes;
            if (_.isArray(normCodes)) {
                shapes = _.map(normCodes, getShape, this);
            } else {
                shapes = getShape(normCodes);
            }
            var result = new $.Deferred();
            result.resolveWith(null, [shapes]);
            return result;
        },

        save: function (shapes) {
            if (_.isArray(shapes)) {
                _.each(shapes, saveShape, this);
            } else {
                this._saveShape(shapes);
            }
            return emptyResolvedPromise();
        },

        clear: function () {
            cache = {};
            return emptyResolvedPromise();
        }

    };

    return mapShapesHashStore;
});


exports.shapesStoreService = ["shapesIndexedDbStore", "shapesHashStore",
    function (mapShapesIndexedDbStore, mapShapesHashStore) {
        return _.isUndefined(indexedDB) ? mapShapesHashStore : mapShapesIndexedDbStore;
    }
];
