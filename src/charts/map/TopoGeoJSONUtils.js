"use strict";

var _ = require('lodash');

exports.inject = function(map) {
    map.factory('topoGeoJSONUtils', exports.topoGeoJSONUtils);
    return exports.topoGeoJSONUtils;
};

exports.topoGeoJSONUtils = function () {
    var topoGeoJSONUtils = {

        /**
         * Convert a list of shapes to geoJSON
         * @param shapeList
         * @param [extraProperties]
         * @returns GeoJSON
         */
        shapeListToGeoJson: function (shapeList, extraProperties) {
            var result = {
                type: "FeatureCollection"
            };
            result.features = _.chain(shapeList)
                .compact()
                .map(function (item) {
                    var feature = {
                        type: "Feature",
                        id: item.normCode,
                        properties: {
                            normCode: item.normCode
                        },
                        geometry: {
                            type: item.geometryType || "Polygon",
                            coordinates: item.shape
                        }
                    };
                    if (extraProperties) {
                        _.extend(feature.properties, extraProperties);
                    }
                    return feature;
                })
                .value();
            return result;
        },

        /**
         * Convert a Co of shapes to geoJSON
         * @param shapeList
         * @param [extraProperties]
         * @returns GeoJSON
         */
        shapeListToTopoJson: function (shapeList) {
            return _.map(shapeList, function (collectionItem) {
                var result = {
                    type: collectionItem.geometryType
                };
                result.objects =  collectionItem.shapes.objects;
                result.arcs =  collectionItem.shapes.arcs;
                result.bbox =  collectionItem.shapes.bbox;
                result.transform =  collectionItem.shapes.transform;
                return result;
            })
        },

        /**
         * Convert a geoJSON to a shapeList
         * @param geoJson
         * @returns Array
         */
        geoJsonToShapeList: function (geoJson) {
            return _.map(geoJson.features, function (feature) {
                return {
                    normCode: feature.properties.normCode,
                    geometryType: feature.geometry.type,
                    shape: feature.geometry.coordinates,
                    hierarchy: this.hierarchyByNormCode(feature.properties.normCode)
                };
            }, this);
        },

        /**
         * Convert a geoJSON to a shapeList
         * @param geoJson
         * @returns Array
         */
        topoJsonToShapeList: function (topoJson, collectionNormCode) {
            return {
                normCode: collectionNormCode,
                geometryType: topoJson.type,
                shapes: {
                    objects: topoJson.objects,
                    arcs: topoJson.arcs,
                    bbox: topoJson.bbox,
                    transform: topoJson.transform
                },
                hierarchy: this.hierarchyByNormCode(collectionNormCode)
            };
        },

        /**
         * Extract granularity form normcode
         * @param {String} normCode
         * @returns {string}
         */
        granularityByNormCode: function (normCode) {
            var granularity = normCode.substring(normCode.lastIndexOf("_") + 1);
            return granularity;
        },

        /**
         * Granularity order by normcode
         * @param normCode
         * @returns {number}
         */
        hierarchyByNormCode: function (normCode) {
            var hierarchy = -1;
            if (normCode) {
                var granularities = ["WORLD", "CONTINENT", "COUNTRY", "ADM-LEVEL-1", "ADM-LEVEL-2", "ISLAND", "ADM-LEVEL-3", "DISTRICT", "SECTION"];
                var granularity = this.granularityByNormCode(normCode);
                hierarchy = _.indexOf(granularities, granularity);
            }
            return hierarchy;
        },

        /**
         * Extract coordinates from a geoJSON
         * @param features
         * @returns coordinates
         */
        getCoordinates: function (features) {
            var coordinates = _.map(features, function (feature) {
                return feature.geometry.coordinates;
            });
            coordinates = _.chain(coordinates).compact().flatten().flatten().flatten().value();
            return coordinates;
        },

        /**
         * Extract the bounding box of a geoJSON
         * @param features
         * @returns BoundingBox
         */
        getBBox: function (features) {
            var coordinates = topoGeoJSONUtils.getCoordinates(features);
            var xCoordinates = _.filter(coordinates, function (num, i) {
                return i % 2 === 0;
            });
            var yCoordinates = _.filter(coordinates, function (num, i) {
                return i % 2 !== 0;
            });
            var bbox = {
                maxX: _.max(xCoordinates),
                minX: _.min(xCoordinates),
                maxY: _.max(yCoordinates),
                minY: _.min(yCoordinates)
            };
            return bbox;
        },

        getBBoxCenter: function (bbox) {
            var originX = bbox.minX + (bbox.maxX - bbox.minX) / 2;
            var originY = bbox.minY + (bbox.maxY - bbox.minY) / 2;
            return [originX, originY];
        },

        /**
         * Extract coordinates from a geoJSON in pixels from path
         * @param features, path
         * @returns coordinates
         */
        getPixelCoordinates: function (features, path) {
            var pixelCoordinates = _.map(features, function (feature) {
                return path.bounds(feature);
            });
            pixelCoordinates = _.chain(pixelCoordinates).compact().flatten().value();
            return pixelCoordinates;
        },

        /**
         * Extract the bounding box of a geoJSON in pixels
         * @param features, path
         * @returns BoundingBox
         */
        getBBoxPixels: function (features, path) {
            var coordinates = topoGeoJSONUtils.getPixelCoordinates(features, path);
            var xCoordinates = _.filter(coordinates, function (num, i) {
                return i % 2 === 0;
            });
            var yCoordinates = _.filter(coordinates, function (num, i) {
                return i % 2 !== 0;
            });
            var bbox = {
                maxX: _.max(xCoordinates),
                minX: _.min(xCoordinates),
                maxY: _.max(yCoordinates),
                minY: _.min(yCoordinates)
            };
            return bbox;
        }

    };
    return topoGeoJSONUtils;
};


