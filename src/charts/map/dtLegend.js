'use strict';

var d3 = require('d3');
var _ = require('lodash');

exports.inject = function (legend) {
    legend.directive('dtLegend', exports.dtLegend);
    return exports.dtLegend;
};

exports.dtLegend = ['$window', '$timeout', function ($window, $timeout) {
    var widthPercent = 0.15;
    var heightPercent = 0.3;
    var top = 8, left = 8;
    var idealWidth, idealHeigth;
    var idealParentWidth, idealParentHeigth;

    function restetLegendPosition(g) {
        g.attr("transform", 'translate(' + left + ', ' + top +')');
    }

    function resetLegendShadow(g) {
        var defsLegend = g.append("defs");

        var filterLegend = defsLegend.append("filter")
            .attr("id", "legend-shadow");

        filterLegend.append("feGaussianBlur")
            .attr("in", "SourceAlpha")
            .attr("stdDeviation", 3)
            .attr("result", "blur");

        filterLegend.append("feOffset")
            .attr("in", "blur")
            .attr("dx", 0)
            .attr("dy", 2)
            .attr("result", "offsetBlur");

        filterLegend.append("feFlood")
            .attr("flood-color", "#070707")                         //Shadow color with opacity
            .attr("flood-opacity", 0.4)
            .attr("result", "offsetColor");

        filterLegend.append("feComposite")
            .attr("in", "offsetColor")
            .attr("in2", "offsetBlur")
            .attr("operator", "in")
            .attr("result", "offsetBlur");

        var feMergeLegend = filterLegend.append("feMerge");

        feMergeLegend.append("feMergeNode")
            .attr("in", "offsetBlur")
        feMergeLegend.append("feMergeNode")
            .attr("in", "SourceGraphic");

        g.style("filter", "url(#legend-shadow)");
    }

    return {
        restrict: 'EA',
        //http://stackoverflow.com/questions/13641105/including-svg-template-in-angularjs-directive/27675374#27675374
        templateNamespace: 'svg',
        replace: true,
        templateUrl: 'map/dt-legend.html',
        scope: {
            addToSvg: '@',
            bboxMargin: '=',
            items: '&items',
            oblong: '='
        },
        link: function(scope, el) {
            if (scope.oblong){
                scope.items = _.chunk(scope.items(), 2);
            } else {
                scope.items = scope.items();
            }

            var renderTimeout;

            var svg = d3.select(scope.addToSvg);

            var parentWidth = svg.node().offsetWidth,
                parentHeight = svg.node().offsetHeight;
            idealWidth = parentWidth * widthPercent;
            idealHeigth = parentHeight * heightPercent;
            idealParentWidth = parentWidth;
            idealParentHeigth = parentHeight;

            var g = svg.select("g.dt-legend");

            $window.onresize = function() {
                scope.$apply();
            };

            scope.$watch(function() {
                return angular.element($window)[0].innerWidth;
            }, function() {
                scope.render();
            });

            scope.$watch(function() {
                return angular.element($window)[0].innerHeight;
            }, function() {
                scope.render();
            });

            scope.render = function() {

                if (renderTimeout) clearTimeout(renderTimeout);


                renderTimeout = $timeout(function () {
                    // resize rects
                    var bboxContentLegend = angular.element( document.querySelector( '.legend-content' ) )[0].getBBox();
                    _.each(angular.element( document.querySelector( '.full-size-rect' ) ), function (rect) {
                        rect.setAttributeNS(null, "width", bboxContentLegend.width + 20);
                        rect.setAttributeNS(null, "height", bboxContentLegend.height + 20);
                    });

                    restetLegendPosition(g);
                    resetLegendShadow(g);
                }, 0);
            }

        }
    }
}];