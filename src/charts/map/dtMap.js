'use strict';

var d3 = require('d3');
var _ = require('lodash');
var topojson = require('topojson');

exports.inject = function(map) {
    require('./MapConfiguration').inject(map);
    require('./TopoGeoJSONUtils').inject(map);
    require('./ShapesApiService').inject(map);
    require('./ShapesStoreService').inject(map);
    require('./ShapesService').inject(map);
    map.directive('dtMap', exports.dtMap)
        .value('canaryFeaturesData', {
            canariasId: "CANARIAS_ADM-LEVEL-1",
            tenerifeProvinceId: "SANTA_CRUZ_DE_TENERIFE_ADM-LEVEL-2",
            laspalmasProvinceId: "LAS_PALMAS_ADM-LEVEL-2",
            tenerifeId: "TENERIFE_ISLAND",
            palmaId: "LA_PALMA_ISLAND",
            gomeraId: "LA_GOMERA_ISLAND",
            hierroId: "EL_HIERRO_ISLAND",
            lanzaroteId: "LANZAROTE_ISLAND",
            fuerteventuraId: "FUERTEVENTURA_ISLAND",
            grancanariaId: "GRAN_CANARIA_ISLAND"
        })
        .value('canaryTranslateCoordinates', {
            translateCoordinateX: 5,
            translateCoordinateY: 6
        })
        .value('componentsBBoxMargin', 0.01)
        .value('mapAdmLevels', {
            admLevel1: 'ADM-LEVEL-1',
            admLevel2: 'ADM-LEVEL-2',
            admLevel3: 'ADM-LEVEL-3'
        });
    return exports.dtMap;
};

var dtMap = function ($window, $timeout, componentsBBoxMargin, shapesService, mapAdmLevels, $http, $q, mapConfiguration) {
    function preprocesDataTotalMsgToCirclesVolumen(items, features, granularity, brands) {
        return _.map(features, function (feature) {
            return {
                id: feature.id,
                value: granularity === "ADM-LEVEL-1" ? _.sum(_.pluck(_.where(items.disgregated, { 'CCAA': feature.properties.normCode}), 'totalMsg'))                //array CCAA and provinces start in 1, the first time is wrong but i need refactor this to norm_code
                    : _.sum(_.pluck(_.where(items.disgregated, { 'province': feature.properties.normCode }), 'totalMsg')),
                color: granularity === "ADM-LEVEL-1" ? _.result(_.find(brands, { 'brand': _.result(_.find(items.disgregated, { 'CCAA': feature.properties.normCode }), 'brand') }), 'color')
                    : _.result(_.find(brands, { 'brand': _.result(_.find(items.disgregated, { 'province': feature.properties.normCode }), 'brand') }), 'color')
            }
        });
    }

    function dropCeutaAndMelillaData(data) {
        return _.filter(data, function(item) {
            return item.CCAA !== 'CIUDAD_AUTONOMA_DE_CEUTA_ADM-LEVEL-1' && item.CCAA !== 'CIUDAD_AUTONOMA_DE_MELILLA_ADM-LEVEL-1';
        })
    }

    function preprocesDataPosMsgPerBrandToCirclesVolumen(items, features, granularity, brands) {
        //fieldIterate = "brand", cuantityField = "posMsg"
        if (granularity === "ADM-LEVEL-1") {
            items.disgregated = dropCeutaAndMelillaData(items.disgregated);
        }
        return _.map(features, function (feature) {
            var maxBrandValue = _.max(
                _.pairs(
                    _.mapValues(
                        _.groupBy(granularity === "ADM-LEVEL-1" ? _.where(items.disgregated, { 'CCAA': feature.properties.normCode}) :
                            _.where(items.disgregated, { 'province': feature.properties.normCode }), _.property('brand')), function (value) {
                            return _.sum(_.pluck(value, 'posMsg'));
                        }
                    )
                ), function (pair) {
                    return pair[1]
                }
            )
            return {
                id: feature.id,
                value: _.last(maxBrandValue),
                color: _.result(_.find(brands, { 'brand': _.first(maxBrandValue) }), 'color')
            }
        });
    }

    return {
        restrict: 'EA',
        templateUrl: 'map/dt-map.html',
        scope: {
            brands: '=',
            items: '='
        },
        //http://www.undefinednull.com/2014/07/07/practical-guide-to-prelink-postlink-and-controller-methods-of-angular-directives/
        //we nedd compile parent before child to solve d3 append order
        link: {
            pre: function (scope, ele) {
                //http://bl.ocks.org/mbostock/4707858
                //http://bl.ocks.org/mbostock/2206590

                scope.componentsBBoxMargin = componentsBBoxMargin;

                var renderTimeout, province, ccaa;
                scope.level = mapAdmLevels.admLevel1;

                var width = d3.select(ele[0].getElementsByClassName('svg-container')[0]).node().offsetWidth,
                    height = d3.select(ele[0].getElementsByClassName('svg-container')[0]).node().offsetHeight;

                var titleMarginTop = 30 + 1 + 15;   //Resume title margin top + border top + text height / 2
                scope.titlePositionx = width - 22;
                scope.titlePositiony = titleMarginTop;

                // Create a unit projection.
                scope.projection = d3.geo.mercator();

                var path = d3.geo.path()
                    .projection(scope.projection);

                var svg = d3.select(ele.find("svg")[0]);

                var g = svg.select("g.perception-chart")
                    .attr("pointer-event","none");
                var h = svg.select("g.features-chart");

                //FIXME: refactor to directive
                //http://techslides.com/demos/d3/d3-world-map-mouse-zoom.html
                var tooltip = d3.select('.svg-container').append("div")
                    .attr("class", "tooltip");

                $window.onresize = function () {
                    scope.$apply();
                };

                scope.$watch('items', function () {
                    scope.volumenCirclesData = preprocesDataPosMsgPerBrandToCirclesVolumen(scope.items, scope.features, scope.level, scope.brands);
                });

                scope.$watch(function () {
                    return angular.element($window)[0].innerWidth;
                }, function () {
                    scope.render();
                });

                scope.$watch(function () {
                    return angular.element($window)[0].innerHeight;
                }, function () {
                    scope.render();
                });

                scope.$on('map:reset-adm-level', function(event, levelTarget) {
                    scope.level = levelTarget;
                    scope.reset();
                });

                scope.render = function () {
                    g.selectAll('*').remove();
                    h.selectAll('*').remove();
                    svg.selectAll('#drop-shadow').remove();

                    if (renderTimeout) clearTimeout(renderTimeout);

                    renderTimeout = $timeout(function () {

                        width = d3.select(ele[0].getElementsByClassName('svg-container')[0]).node().offsetWidth;
                        height = d3.select(ele[0].getElementsByClassName('svg-container')[0]).node().offsetHeight;

                        applyDropShadows();
                        loadSpainFeatures().then(function () {
                            scope.features = topojson.feature(scope.containerCCACollection, scope.containerCCACollection.objects.collection).features;
                            scope.volumenCirclesData = preprocesDataPosMsgPerBrandToCirclesVolumen(scope.items, scope.features, scope.level, scope.brands);
                            var regions = topojson.merge(scope.containerCCACollection, scope.containerCCACollection.objects.collection.geometries);       //To calculate bounds

                            scope.projection
                                .scale(1)
                                .translate([0, 0]);

                            var b = path.bounds(regions);
                            scope.s = .90 / Math.max((b[1][0] - b[0][0]) / width, (b[1][1] - b[0][1]) / height);
                            scope.t = [(width - scope.s * (b[1][0] + b[0][0])) / 2, (height - scope.s * (b[1][1] + b[0][1])) / 2];

                            scope.projection
                                .scale(scope.s)
                                .translate(scope.t);

                            addSapainCCAA();
                            addSpainCountryContour();
                            addSpainCountryMesh();
                        });

                        function addSpainCountryContour() {
                            g.insert("path")
                                .datum(topojson.feature(scope.containerSpainCollection, scope.containerSpainCollection.objects.collection))
                                .attr("class", "contour-spain-country")
                                .attr("d", path)
                                .attr("pointer-events","none")
                                .style("filter", "url(#drop-shadow)");
                        }

                        function addSapainCCAA() {
                            h.append("g")
                                .attr("id", "ccaas")
                                .selectAll("path")
                                .data(scope.features)
                                .enter()
                                .append("path")
                                .attr("id", function(d) { return d.id; })
                                .attr("d", path)
                                .style("fill","none")
                                .style("pointer-events","all")
                                .attr('cursor','pointer')
                                .on("click", ccaaClicked)
                                .on("mouseover",function(d,i) {
                                    d3.select(this).style('fill','#e6e6e6');
                                    d3.select(this).style('fill-opacity', .7);

                                })
                                .on("mouseout",function(d,i){
                                    d3.select(this).style('fill','none');
                                    //FIXME: refactor to directive
                                    tooltip.classed("hidden", true);
                                })
                                .on("mousemove",function(d,i){
                                    var mouse = d3.mouse(svg.node()).map( function(d) { return parseInt(d); } );
                                    //FIXME: refactor to directive
                                    tooltip
                                        .classed("hidden", false)
                                        .attr("style", "left:"+(mouse[0]+25)+"px;top:"+mouse[1]+"px")
                                        .html(d.properties.name);
                                });
                        }

                        function addSpainCountryMesh() {
                            g.insert("path")
                                .datum(topojson.mesh(scope.containerCCACollection, scope.containerCCACollection.objects.collection, function(a, b) { return a !== b; }))
                                .attr("class", "mesh-spain-countr")
                                .attr("d", path)
                                .attr("pointer-events","none");
                        }

                        function applyDropShadows () {
                            var filter = svg.append("defs")
                                .append("filter")
                                .attr("id", "drop-shadow")
                                .attr("x", "0")
                                .attr("y", "0")
                                .attr("width", "200%")
                                .attr("height", "200%");

                            filter.append("feOffset")
                                .attr("result", "offOut")
                                .attr("in", "SourceGraphic")
                                .attr("dx", 3)
                                .attr("dy", 3);

                            filter.append("feColorMatrix")
                                .attr("result", "matrixOut")
                                .attr("in", "offOut")
                                .attr("color-interpolation-filters", "sRGB")
                                .attr("type", "matrix")
                                .attr("values", ".251 .251 .251 0 0 " +
                                                ".251 .251 .251 0 0 " +
                                                ".251 .251 .251 0 0 " +
                                                "1 1 1 0 0");

                            filter.append("feGaussianBlur")
                                .attr("result", "blurOut")
                                .attr("in", "matrixOut")
                                .attr("stdDeviation", 3);

                            filter.append("feBlend")
                                .attr("in", "SourceGraphic")
                                .attr("in2", "blurOut")
                                .attr("mode", "normal");
                        }

                        function removeCCAALayer () {
                            h.selectAll("#ccaas").remove();
                            g.selectAll(".contour-spain-country").remove();
                            g.selectAll(".mesh-spain-countr").remove();
                        }

                        function addSelectedCCAAContour(ccaaId) {
                            var ccaas = topojson.feature(scope.containerCCACollection, scope.containerCCACollection.objects.collection),
                                selectedCCAA = ccaas.features.filter(function(d) {
                                    return d.id === ccaaId;
                                })[0];

                            g.insert("path")
                                .datum(selectedCCAA)
                                .attr("class", "contour-selected-ccaa")
                                .attr("d", path)
                                .attr("pointer-events","none")
                                .style("filter", "url(#drop-shadow)");
                        }

                        function addProvincesToSelectedCCAA () {
                            var provincesGroup = h.append("g")
                                .attr("id", "provinces");

                            _.each(topojson.feature(scope.containerProvincesCollection, scope.containerProvincesCollection.objects.collection).features, function (geometryProvince) {
                                provincesGroup.insert("path")
                                    .datum(geometryProvince)
                                    .attr("id", function (d){
                                        return "province-" + d.id;
                                    })
                                    .attr("d", path)
                                    .style("fill","none")
                                    .style("pointer-events","all")
                                    .attr('cursor','pointer')
                                    .on("mouseover", function(d,i) {
                                        d3.select(this).style('fill','#e6e6e6');
                                        d3.select(this).style('fill-opacity', .7);

                                    })
                                    .on("mouseout",function(d,i){
                                        d3.select(this).style('fill','none');
                                        //FIXME: refactor to directive
                                        tooltip.classed("hidden", true)
                                    })
                                    .on("mousemove", function(d,i){
                                        var mouse = d3.mouse(svg.node()).map( function(d) { return parseInt(d); } );
                                        //FIXME: refactor to directive
                                        tooltip
                                            .classed("hidden", false)
                                            .attr("style", "left:"+(mouse[0]+25)+"px;top:"+mouse[1]+"px")
                                            .html(d.properties.name);        //Get CCAA by norm_code
                                    })
                                    .on("click", provinceClicked);
                            })
                        }

                        function addSelectedCCAAMesh() {
                            g.insert("path")
                                .datum(topojson.mesh(scope.containerProvincesCollection, scope.containerProvincesCollection.objects.collection,
                                    function(a, b) {
                                        return a !== b;
                                    }))
                                .attr("class", "mesh-selected-ccaa-contour")
                                .attr("d", path)
                                .attr("pointer-events","none");
                        }

                        function filterFeaturesCCAASelected(features, caaNormcode) {
                            var itemsSelectedCCAA = _.pluck(_.where(scope.items.disgregated, { 'CCAA': caaNormcode}), 'province');

                            if (!_.isEmpty(itemsSelectedCCAA)) {
                                return _.filter(features, function (feature) {
                                    return _.includes(itemsSelectedCCAA, feature.properties.normCode);
                                });
                            } else {
                                return;
                            }

                        }

                        function changeAdmLevel(levelTarget, featureId, currentLabel) {
                            tooltip.classed("hidden", true);
                            scope.$emit('map:change-adm-level', levelTarget, featureId, currentLabel);
                        }
                        function containsOnlyAProvince() {
                            return _.size(scope.containerProvincesCollection.objects.collection.geometries) === 1;
                        }

                        function ccaaClicked(d) {
                            var volumeCircles = d3.select("g.dt-volumen-circles");

                            volumeCircles.selectAll('circle')
                                .transition()
                                .duration(750)
                                .attr("r", 0);

                            loadSelectedCCAAProvincesFeatures(d.properties.normCode).then(function () {
                                removeCCAALayer();

                                g.selectAll("#provinces").remove();
                                province = null;
                                ccaa = d;

                                addProvincesToSelectedCCAA();
                                addSelectedCCAAContour(ccaa.id);
                                addSelectedCCAAMesh();

                                var zoomData = getScaleAndTranslate(d);
                                zoom(zoomData);

                                if (containsOnlyAProvince()) {
                                    provinceClicked(_.first(scope.containerProvincesCollection.objects.collection.geometries));
                                } else {
                                    scope.level = mapAdmLevels.admLevel2;
                                    changeAdmLevel(scope.level, ccaa.properties.normCode, ccaa.properties.name);
                                }
                                var featuresSelectedCCAA = filterFeaturesCCAASelected(topojson.feature(scope.containerProvincesCollection, scope.containerProvincesCollection.objects.collection).features, ccaa.properties.normCode);
                                if (!_.isEmpty(featuresSelectedCCAA)) {
                                    scope.features = featuresSelectedCCAA;
                                    scope.volumenCirclesData = preprocesDataPosMsgPerBrandToCirclesVolumen(scope.items, scope.features, scope.level, scope.brands);
                                } else {
                                    scope.volumenCirclesData = [];
                                }
                            });
                            scope.$apply();
                        }

                        function getScaleAndTranslate(d) {
                            var bounds = path.bounds(d),
                                dx = bounds[1][0] - bounds[0][0],
                                dy = bounds[1][1] - bounds[0][1],
                                x = (bounds[0][0] + bounds[1][0]) / 2,
                                y = (bounds[0][1] + bounds[1][1]) / 2;
                                scope.scale = .9 / Math.max(dx / width, dy / height);
                                scope.translate = [width / 2 - scope.scale * x, height / 2 - scope.scale * y];

                            scope.selected = d;
                            scope.s = scope.scale;
                            scope.t = scope.translate;

                            return [scope.scale, scope.translate];
                        }

                        function zoom(xyz) {
                            var scale = xyz[0];
                            var translate = xyz[1];
                            g.transition()
                                .duration(750)
                                .style("stroke-width", 0.5 / scale + "px")
                                .attr("transform", "translate(" + translate + ")scale(" + scale + ")");
                            h.transition()
                                .duration(750)
                                .style("stroke-width", 0.5 / scale + "px")
                                .attr("transform", "translate(" + translate + ")scale(" + scale + ")");
                        }

                        function provinceClicked(current) {
                            scope.level = mapAdmLevels.admLevel3;
                            changeAdmLevel(scope.level, current.properties.normCode, current.properties.name);
                            //Disable rest of provinces
                            g.selectAll('.contour-selected-ccaa')
                                .attr('class', 'disabled-caa-contour');

                            //hide rest of clickable provinces and change color to selected
                            h.select('#provinces')
                                .selectAll('path')
                                .style('display', "none");

                            var provinces = topojson.feature(scope.containerProvincesCollection, scope.containerProvincesCollection.objects.collection),
                                provinceSelected = provinces.features.filter(function(d) { return d.id === current.id; })[0];

                            g.append("path")
                                .datum(provinceSelected)
                                .attr("class", "selected-province")
                                .attr("d", path)
                                .attr('fill', '#e6e6e6');
                        }

                        function removeProvincesLayer () {
                            h.selectAll("#provinces").remove();
                            g.selectAll(".contour-selected-ccaa").remove();
                            g.selectAll(".mesh-selected-ccaa-contour").remove();
                        }

                        function resetToCCAALayer () {
                            g.selectAll('.disabled-caa-contour')
                                .attr('class', 'contour-selected-ccaa');

                            h.select('#provinces')
                                .selectAll('path')
                                .style('display', "");

                            g.selectAll('.selected-province').remove();
                        }

                        scope.reset = function () {

                            resetToCCAALayer();
                            if (containsOnlyAProvince()) {
                                scope.level = mapAdmLevels.admLevel2;
                            }
                            if (scope.level === mapAdmLevels.admLevel2) {
                                scope.level = mapAdmLevels.admLevel1;
                                changeAdmLevel(scope.level);
                                $timeout(function () {
                                    removeProvincesLayer();
                                    addSapainCCAA();
                                    addSpainCountryContour();
                                    addSpainCountryMesh();
                                }, 750);

                                scope.features = topojson.feature(scope.containerCCACollection, scope.containerCCACollection.objects.collection).features;
                                scope.volumenCirclesData = preprocesDataPosMsgPerBrandToCirclesVolumen(scope.items, scope.features, scope.level, scope.brands);

                                h.transition()
                                    .duration(750)
                                    .style("stroke-width", "0.5px")
                                    .attr("transform", "");

                                g.transition()
                                    .duration(750)
                                    .style("stroke-width", "0.5px")
                                    .attr("transform", "");

                                //TODO: refactor this, look at porcentual way
                                scope.$broadcast('reset-adm-level-1');
                            } else if (scope.level === mapAdmLevels.admLevel3) {
                                scope.level = mapAdmLevels.admLevel2;
                                changeAdmLevel(scope.level, undefined, ccaa.properties.name);
                                //resetToCCAALayer();
                            }
                        }
                    }, 0);

                    var loadSpainFeatures = function () {
                        var defered = $q.defer();
                        var promise = defered.promise;

                        var loadSpainCCAAAncestors = function () {
                            return $http.get(mapConfiguration.endpoint.ancestors + '/country/ESPANA_COUNTRY')
                        },
                        shapesParallelLoad = function (normCodes) {
                            return $q.all([
                                        shapesService.fetchShapesCollection(normCodes.data, 'ESPANA_ADM-LEVEL-1'),   //When merge a country name with ADM-LEVE-1 this represent a collection
                                        shapesService.fetchShapesCollection(['ESPANA_COUNTRY', 'CANARIAS_ADM-LEVEL-1', 'ISLAS_BALEARES_ADM-LEVEL-1'], 'ESPANA_COUNTRY')   //When merge a country name with ADM-LEVE-1 this represent a collection
                            ])
                            .then(function(arrayOfResponses) {
                                scope.containerCCACollection = _.first(arrayOfResponses[0]);
                                scope.containerSpainCollection = _.first(arrayOfResponses[1]);
                                scope.canaryBounds = path.bounds(topojson.feature(scope.containerCCACollection, scope.containerCCACollection.objects.collection).features.filter(function(d) {
                                                return "CANARIAS_ADM-LEVEL-1" == d.properties.normCode;
                                            })[0]);
                                defered.resolve();
                            })
                        },
                        reportErros = function (error) {
                            console.log(error);
                        };

                        loadSpainCCAAAncestors()
                            .then(shapesParallelLoad)
                            .catch(reportErros);

                        return promise;
                    }

                    var loadSelectedCCAAProvincesFeatures = function (normCode) {
                        var defered = $q.defer();
                        var promise = defered.promise;

                        var loadSpainProvincesAncestors = function () {
                            return $http.get(mapConfiguration.endpoint.ancestors + '/admLevel1/' + normCode)
                        },
                        shapesParallelLoad = function (normCodes) {
                            return $q.all([
                                shapesService.fetchShapesCollection(normCodes.data, normCode),
                            ])
                            .then(function(arrayOfResponses) {
                                scope.containerProvincesCollection = _.first(arrayOfResponses[0]);
                                defered.resolve();
                            })
                        },
                        reportErros = function (error) {
                            console.log(error);
                        };

                        loadSpainProvincesAncestors()
                            .then(shapesParallelLoad)
                            .catch(reportErros);

                        return promise;
                    }
                }
            }
        }
    };
}
exports.dtMap = ['$window', '$timeout', 'componentsBBoxMargin', 'shapesService', 'mapAdmLevels', '$http', '$q', 'mapConfiguration', dtMap];