var d3 = require('d3');
var _ = require('lodash');

exports.inject = function (map) {
    map.directive('dtMapCanaryFictitiousLine', exports.dtMapCanaryFictitiousLine);
    return exports.dtMapCanaryFictitiousLine;
};

exports.dtMapCanaryFictitiousLine = ['$window', '$timeout', 'mapAdmLevels', function ($window, $timeout, mapAdmLevels) {
    return {
        restrict: 'EA',
        templateNamespace: 'svg',
        replace: true,
        scope: {
            parentSvg: '@',
            canaryBounds: '=',
            admLevel: '='
        },
        link: function link(scope) {
            var renderTimeout;
            var topDistance = 40, rightDistance = 10;

            var path = d3.geo.path()
                .projection(d3.geo.mercator());

            var svg = d3.select(scope.parentSvg);

            var parentWidth = svg.node().offsetWidth,
                parentHeight = svg.node().offsetHeight;

            $window.onresize = function () {
                scope.$apply();
            };

            scope.$watch(function () {
                return angular.element($window)[0].innerWidth || angular.element($window)[0].innerHeight;
            }, function () {
                svg.selectAll('.canary-ficticious').remove();
            });

            scope.$watch('canaryBounds', function(newVals, oldVals) {
                if (newVals !== oldVals) {
                    $timeout(function () {
                        scope.render();
                    }, 750);    //Same as reset transition
                }
            }, true);

            scope.$watch('admLevel', function(newVals, oldVals) {
                if (newVals !== oldVals) {
                    if (scope.admLevel === mapAdmLevels.admLevel1) {
                        $timeout(function () {
                            scope.render();
                        }, 750);    //Same as reset transition
                    } else {
                        scope.render();
                    }
                }
            }, false);

            scope.render = function () {
                svg.selectAll('.canary-ficticious').remove();

                if (renderTimeout) clearTimeout(renderTimeout);

                renderTimeout = $timeout(function () {
                    if (scope.admLevel === mapAdmLevels.admLevel1) {
                        parentWidth = svg.node().offsetWidth,
                            parentHeight = svg.node().offsetHeight;

                        addCanaryFictitiousLine();

                        function addCanaryFictitiousLine() {
                            var canaryPerimeter = scope.canaryBounds;
                            var x0 = canaryPerimeter[0][0], y0 = canaryPerimeter[0][1] - topDistance;
                            var x1 = canaryPerimeter[1][0] - rightDistance, y1 = canaryPerimeter[0][1] - topDistance;
                            svg.insert("path")
                                .attr("class", "canary-ficticious")
                                .attr("d", function() {
                                    return "M" + x0 + "," + y0
                                        + "L" + x1 + "," + y1
                                        + "z";
                                });

                            svg.insert("path")
                                .attr("class", "canary-ficticious")
                                .attr("d", function() {
                                    return "M" + x1 + "," + y1
                                        + "L" + (x1 + ((x1-x0) / 2) - topDistance) + "," + (y1 + ((canaryPerimeter[1][1] - canaryPerimeter[0][1]) / 3 * 2))
                                        + "z";
                                });

                        }
                    }
                }, 0);
            };
        }
    };
}];
