var d3 = require('d3');
var _ = require('lodash');

exports.inject = function (grid) {
    grid.directive('dtMapGrid', exports.dtMapGrid);
    return exports.dtMapGrid;
};

exports.dtMapGrid = ['$window', '$timeout', function ($window, $timeout) {
    function makeGridData(gridWidth, gridHeight, square) {
        var data = new Array();
        var gridItemWidth = 40;
        var gridItemHeight = square ? gridItemWidth : gridHeight / 7;
        var numberOfColumns = gridWidth / gridItemWidth;
        var numberOfRows = gridHeight / gridItemHeight;
        var startX = 0;
        var startY = 0;
        var stepX = gridItemWidth;
        var stepY = gridItemHeight;
        var xpos = startX;
        var ypos = startY;
        var newValue = 0;
        var count = 0;

        for (var index_a = 0 ; index_a < numberOfRows.toFixed() ; index_a++) {
            data.push(new Array());
            for (var index_b = 0 ; index_b < numberOfColumns.toFixed() + 1 ; index_b++) {
                newValue = Math.round(Math.random() * (100 - 1) + 1);
                data[index_a].push({
                    width: gridItemWidth,
                    height: gridItemHeight,
                    x: xpos,
                    y: ypos,
                    count: count
                });
                xpos += stepX;
                count += 1;
            }
            xpos = startX;
            ypos += stepY;
        }
        return data;
    }
    return {
        restrict: 'EA',
        templateNamespace: 'svg',
        replace: true,
        scope: {
            addToG: '@',
            parentSvg: '@'
        },
        link: function link(scope) {
            var renderTimeout;
            var griLinesColor = '#c9c9c9';

            var svg = d3.select(scope.parentSvg);
            var g = d3.select(scope.addToG);

            var parentWidth = svg.node().offsetWidth,
                parentHeight = svg.node().offsetHeight;

            $window.onresize = function () {
                scope.$apply();
            };

            scope.$watch(function () {
                return angular.element($window)[0].innerWidth || angular.element($window)[0].innerHeight;
            }, function () {
                $timeout(function () {
                    scope.render();
                }, 750);    //Same as reset transition
            });

            scope.render = function () {
                g.selectAll('*').remove();

                if (renderTimeout) clearTimeout(renderTimeout);

                renderTimeout = $timeout(function () {
                    console.log('.' + scope.parentSvg);
                    parentWidth = svg.node().offsetWidth === undefined ?  jQuery(scope.parentSvg).width() : svg.node().offsetWidth,
                    parentHeight = svg.node().offsetHeight === undefined ? jQuery(scope.parentSvg).height() : svg.node().offsetHeight;

                    addBackgroundRect();

                    var calData = makeGridData(parentWidth, parentHeight, true);
                    var row = g.selectAll('.row').data(calData).enter().append('g:g').attr('class', 'row');

                    addGrid();

                    function addGrid() {
                        row.selectAll('.cell').data(function (d) {
                            return d;
                        }).enter().append('g:rect').attr('class', 'cell').attr('x', function (d) {
                            return d.x;
                        }).attr('y', function (d) {
                            return d.y;
                        }).attr('width', function (d) {
                            return d.width;
                        }).attr('height', function (d) {
                            return d.height;
                        }).style('fill', 'none').style('stroke', griLinesColor);
                    }

                    function addBackgroundRect() {
                        if (g.select('rect.background').empty()) {
                            g.append("rect")
                                .attr("class", "background")
                                .attr("width", parentWidth)
                                .attr("height", parentHeight);
                        }
                    }
                }, 0);
            };
        }
    };
}];
