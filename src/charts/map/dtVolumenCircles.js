'use strict';

var d3 = require('d3');
var _ = require('lodash');

exports.inject = function (volumenCircles) {
    volumenCircles.directive('dtVolumenCircles', exports.dtVolumenCircles);
    return exports.dtVolumenCircles;
};

exports.dtVolumenCircles = ['$window', '$timeout', 'mapAdmLevels', function ($window, $timeout, mapAdmLevels) {
    return {
        restrict: 'EA',
        templateNamespace: 'svg',
        replace: true,
        templateUrl: 'map/dt-volumen-circles.html',
        scope: {
            addToSvg: '@',
            features: '=',
            items: '=',
            projection: '=',
            scale: '=',
            translate: '=',
            admLevel: '=',
            selected: '='
        },
        link: function(scope, el) {
            //http://bl.ocks.org/mbostock/9943478
            var maxRange = 15, minRange = 2;
            var renderTimeout;
            var minVolumenMentions, maxVolumenMentions;

            var path = d3.geo.path()
                .projection(scope.projection);

            var svg = d3.select(scope.addToSvg);

            var g = svg.select("g.dt-volumen-circles");

            $window.onresize = function() {
                scope.$apply();
            };

            scope.$watch(function () {
                return angular.element($window)[0].innerWidth || angular.element($window)[0].innerHeight;
            }, function () {
                $timeout(function () {
                    scope.render();
                }, 750);    //Same as reset transition
            });

            scope.$watch('items', function() {
                g.selectAll('*').remove();
                $timeout(function () {
                    scope.render();
                }, 750);    //Same as reset transition
            }, true);

            scope.$on('reset-adm-level-1', function(event, args) {
                svg.select("g.dt-volumen-circles")
                    .attr("transform", "");
            });

            scope.render = function() {
                g.selectAll('*').remove();

                if (renderTimeout) clearTimeout(renderTimeout);


                renderTimeout = $timeout(function () {
                    if (scope.admLevel === mapAdmLevels.admLevel1 && !_.isUndefined(scope.items) && _.isEmpty(_.first(d3.selectAll('g.dt-volumen-circles circle')))) {
                        renderAdmLevel1();
                    } else if ((scope.admLevel === mapAdmLevels.admLevel2 || scope.admLevel === mapAdmLevels.admLevel3) && !_.isUndefined(scope.items)) {
                        renderAdmLevel2();
                    }
                    function renderAdmLevel1 () {
                        minVolumenMentions = _.min(_.pluck(scope.items, 'value'));
                        maxVolumenMentions = _.max(_.pluck(scope.items, 'value'));
                        var radius = d3.scale.sqrt()
                            .domain([minVolumenMentions, maxVolumenMentions]).range([2, 15]);           //FIXME: set range to max area posible of small region

                        if (!_.isUndefined(scope.features)) {
                            g.selectAll("circle")
                                .data(scope.features)
                                .enter().append("circle")
                                .attr("pointer-events", "none")
                                .attr("transform", function(d) {
                                    return "translate(" + path.centroid(d) + ")";
                                })
                                .attr("fill", function (d) {
                                    return _.result(_.find(scope.items, { 'id': d.id }), 'color');
                                })
                                .attr("r", 0)
                                .transition()
                                .attr("r", function(d) {
                                    return radius(_.result(_.find(scope.items, { 'id': d.id }), 'value'));
                                });
                        }
                    }

                    function renderAdmLevel2 () {
                        svg.select("g.dt-volumen-circles")
                            .attr("transform", "translate(" + scope.translate + ")scale(" + scope.scale + ")");

                        var radius = d3.scale.sqrt();
                        //TODO: proportional circles, update range
                        if (_.size(scope.items) === 1) {
                            radius.domain([minVolumenMentions, maxVolumenMentions]).range([minRange, maxRange]);           //FIXME: set range to max area posible of small region
                        } else {
                            minVolumenMentions = _.min(_.pluck(scope.items, 'value'));
                            maxVolumenMentions = _.max(_.pluck(scope.items, 'value'));
                            radius.domain([minVolumenMentions, maxVolumenMentions]).range([minRange, maxRange]);           //FIXME: set range to max area posible of small region
                        }

                        g.selectAll("circle")
                            .data(scope.features)
                            .enter().append("circle")
                            .attr("pointer-events", "none")
                            .attr('class', function (d) {
                                return "volume-circles-adm-level2 circle" + d.id;
                            })
                            .attr("transform", function(d) {
                                return "translate(" + path.centroid(d) + ")";
                            })
                            .attr("fill", function (d) {
                                return _.result(_.find(scope.items, { 'id': d.id }), 'color');
                            })
                            .attr("r", 0)
                            .transition()
                            .duration(750)
                            .attr("r", function(d) {
                                var calculatedRadius = radius(_.result(_.find(scope.items, { 'id': d.id }), 'value'));
                                //TODO: why not working radius maxrange ????
                                return calculatedRadius > maxRange ? maxRange : calculatedRadius;
                            });
                    }
                }, 0);
            }

        }
    }
}];