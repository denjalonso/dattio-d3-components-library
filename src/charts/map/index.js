'use strict';

var map = module.exports = angular.module('dtcharts.charts.map', []);

require('./dtMap').inject(map);
var dtLegendModule = require('./dtLegend');
dtLegendModule.$inject = ['$window', '$timeout'];
dtLegendModule.inject(map);

var dtMapGrid = require('./dtMapGrid');
dtMapGrid.$inject = ['$window', '$timeout'];
dtMapGrid.inject(map);

var dtMapCanaryFictitiousLine = require('./dtMapCanaryFictitiousLine');
dtMapCanaryFictitiousLine.$inject = ['$window', '$timeout'];
dtMapCanaryFictitiousLine.inject(map);

var dtVolumenCircles = require('./dtVolumenCircles');
dtVolumenCircles.$inject = ['$window', '$timeout'];
dtVolumenCircles.inject(map);

