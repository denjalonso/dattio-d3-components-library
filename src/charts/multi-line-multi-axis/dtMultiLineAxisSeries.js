'use strict';

var d3 = require('d3');
var _ = require('lodash');
var moment = require('moment');

exports.inject = function(multilineAxisSeries) {
    multilineAxisSeries.directive('dtMultiLineAxisSeries', exports.dtMultiLineAxisSeries);
    return exports.dtMultiLineAxisSeries;
};

var dtMultiLineAxisSeries = function ($window, $timeout, $filter) {
    function calculatePercent(number, percent) {
        return number * percent / 100;
    }
    return {
        restrict: 'EA',
        replace: true,
        templateUrl: 'multi-line-multi-axis/dt-multiline-axis-series.html',
        scope: {
            items: '='
        },
        link: {
            pre: function (scope, ele, attrs) {
                var renderTimeout;

                var linesLeftPaddingPercent = 10;
                var linesRightPaddingPercent = 12.17;

                var d3ParseDate = d3.time.format("%d/%m");

                var widthPixels = d3.select(ele[0].getElementsByClassName('svg-container-multiline-axis-series')[0]).node().offsetWidth;
                var heightPixels = d3.select(ele[0].getElementsByClassName('svg-container-multiline-axis-series')[0]).node().offsetHeight;
                var chartMargin = {top: 20, right: 30, bottom: 30, left: 50};

                var svg = d3.select(ele.find("svg")[0]);

                var multilineAxisSeries = svg.append("g")
                    .attr("class", "multiline-axis-series-chart");

                var chart = multilineAxisSeries.append("g")
                    .attr("class", "chart-multilinie-axis-value")
                    .attr("transform", "translate(" + chartMargin.left + "," + chartMargin.top + ")");

                var multilineAxisSeriesTooltip = d3.select('.svg-container-multiline-axis-series').append("div")
                    .attr("class", "multiline-axis-series-tooltip")
                    .style('display', "none");

                $window.onresize = function () {
                    scope.$apply();
                };

                scope.$watch(function () {
                    return angular.element($window)[0].innerWidth || angular.element($window)[0].innerHeight;
                }, function () {
                    $timeout(function () {
                        scope.render();
                    }, 750);    //Same as reset transition
                });

                scope.$watch('items', function(newVals, oldVals) {
                    if (newVals !== oldVals) {
                        $timeout(function () {
                            scope.render();
                        }, 750);    //Same as reset transition
                    }
                }, false);

                scope.render = function () {
                    multilineAxisSeries.selectAll("*").remove();

                    if (renderTimeout) clearTimeout(renderTimeout);

                    renderTimeout = $timeout(function () {

                        widthPixels = d3.select(ele[0].getElementsByClassName('svg-container-multiline-axis-series')[0]).node().offsetWidth;
                        heightPixels = d3.select(ele[0].getElementsByClassName('svg-container-multiline-axis-series')[0]).node().offsetHeight;
                        chartMargin = {
                            top: 20,
                            right: calculatePercent(widthPixels, linesRightPaddingPercent),
                            bottom: 30,
                            left: calculatePercent(widthPixels, linesLeftPaddingPercent)
                        };

                        var width = widthPixels - chartMargin.left - chartMargin.right,
                            height = heightPixels - chartMargin.top - chartMargin.bottom;

                        var itemsOneLevelArray = _.flatten(_.flatten(_.pluck(scope.items, 'data')));
                        var xScaleDomainValues = d3.extent(itemsOneLevelArray, function(d) { return d.day; });
                        var xScaleDomain = [new Date(xScaleDomainValues[0]), new Date(xScaleDomainValues[1])];
                        var x = d3.time.scale()
                            .range([0, width])
                            .domain(xScaleDomain).nice(d3.time.day);

                        var y = d3.scale.linear()
                            .domain([-1, 1])
                            .range([height, 0]);

                        var yAxis;

                        chart = multilineAxisSeries.append("g")
                            .attr("class", "chart-multilinie-axis-value")
                            .attr("transform", "translate(" + chartMargin.left + "," + chartMargin.top + ")");

                        addXAxis();
                        addYAxis();
                        addLineSeries();
                        addPoints();
                        addLabels();

                        function addXAxis() {
                            var tickSize = 12;
                            var xAxis = d3.svg.axis()
                                .scale(x)
                                .orient("bottom")
                                .ticks(d3.time.day, 1)
                                .tickFormat(d3ParseDate)
                                .tickPadding(7)
                                .innerTickSize(tickSize)
                                .outerTickSize(0);

                            chart.append("g")
                                .attr("class", "x-axis")
                                .attr("transform", "translate(0," + height + ")")
                                .call(xAxis)
                                .selectAll("line")
                                .attr("transform", "translate(0, -" + tickSize + ")");
                        }

                        function addYAxis() {
                            yAxis = d3.svg.axis()
                                .scale(y)
                                .orient("left");

                            chart.append("g")
                                .attr("class", "y-axis")
                                .call(yAxis
                                    .ticks(40)
                                    .tickFormat(function (d) {
                                        if (d === 0.65) {
                                            return "Positivo";
                                        } else if (d === 0) {
                                            return "Neutro";
                                        } else if (d === -0.65) {
                                            return "Negativo";
                                        }
                                        return;
                                    }));

                            addLineTicks();
                        }

                        function addLineTicks() {
                            chart.selectAll("line.grid")
                                .data([-0.3, 0.3, 1])
                                .enter().append("line")
                                .attr("class", "grid")
                                .attr("x1", 0)
                                .attr("x2", width)
                                .attr("y1", y)
                                .attr("y2", y);
                        }

                        var drawedData;
                        function addLineSeries () {
                            chart.append("clipPath")
                                .attr("id", "clip-line-axis-series")
                                .append("rect")
                                .attr("width", width)
                                .attr("height", height);

                            var line = d3.svg.line()
                                .interpolate("cardinal")
                                //.interpolate("basis")
                                .x(function(d) { return x(d.day); })
                                .y(function(d) { return y(d.avgPerception); });

                            drawedData = _.pluck(scope.items, 'data');
                            drawedData =_.filter(drawedData, function (item) {
                                return _.some(item, function (subItem) {
                                    return subItem.avgPerception !== 0;
                                });
                            });
                            chart.selectAll('.line-serie')
                                .data(drawedData)
                                .enter()
                                .append("path")
                                .attr("class", "line-serie")
                                .attr("clip-path", "url(#clip-line-axis-series)")
                                .attr('stroke', function(d,i){
                                    return scope.items[i].color;
                                })
                                .attr("d", line);
                        }

                        function addPoints () {
                            var points = chart.selectAll('.dots-lines-series')
                            	.data(drawedData)
                            	.enter()
                            	.append("g")
                                .attr("class", "dots-lines-series");

                            points.selectAll('.dots-lines-series')
                            	.data(function(d, index){
                            		var a = [];
                            		d.forEach(function(point,i){
                            			a.push({'index': index, 'point': point});
                            		});
                            		return a;
                            	})
                            	.enter()
                            	.append('circle')
                            	.attr('class','dot-line')
                            	.attr("r", function (d, i) {
                                    return i === _.first(drawedData).length - 1 ? 6 : 4;
                                })
                            	.attr('fill', function(d,i){
                            		return scope.items[d.index%scope.items.length].color;
                            	})
                            	.attr("transform", function(d) {
                            		return "translate(" + x(d.point.day) + "," + y(d.point.avgPerception) + ")"; }
                            	)
                                .on("mouseout", function(d, i) {
                                    multilineAxisSeriesTooltip.classed("hidden", true)
                                })
                                .on("mousemove", function(d, i) {
                                    updateToolTip(d);
                                });
                        }

                        function updateToolTip(d) {
                            d3.event.preventDefault();
                            var mouse = d3.mouse(svg.node()).map( function(d) { return parseInt(d); } );
                            var section, percent;
                            if (d.point.avgPerception >= -1 && d.point.avgPerception < -0.3) {
                                section = "negativa";
                                percent = (100/0.7) * (d.point.avgPerception + 0.3) * -1;
                            } else if (d.point.avgPerception >= -0.3 && d.point.avgPerception < 0.3) {
                                section = "neutra";
                                percent = (d.point.avgPerception) < 0 ? (100/0.7) * (d.point.avgPerception + 0.3) : (100/0.7) * (d.point.avgPerception + 0.4);
                            } else if (d.point.avgPerception >= 0.3 && d.point.avgPerception <= 1) {
                                section = "positiva";
                                percent = (100/0.7) * (d.point.avgPerception - 0.3);
                            }
                            //FIXME: refactor to directive
                            multilineAxisSeriesTooltip
                                .classed("hidden", false)
                                .attr("style", "left:"+(mouse[0]- 70)+"px;top:"+ (mouse[1]) +"px")
                                .html(
                                "<div style='width: 100%;'>" +
                                    "<div style='position: relative; padding: 10px; border-bottom: 1px solid #e9e9e9; font-size: 10px;'>" +
                                        "<div style='height: 12px; width: 12px; border-radius: 6px; background-color:" + scope.items[d.index%scope.items.length].color + "'></div>" +
                                        "<span style='position: absolute; top: 10px; left: 28px;'>" + scope.items[d.index%scope.items.length].brand + "</span>" +
                                    "</div>" +
                                    "<p style='padding: 10px; width: 90px; height: 70px; margin: 0px; font-size: 12px;'> " +
                                        "<strong style='font-size: 14px;'>" + $filter('number')(percent.toFixed(2)) + "%</strong> " +
                                        "percepción " + section +
                                    "</p>" +
                                "</div>"
                        );        //Get CCAA by norm_code
                        }

                        function addLabels () {
                            chart.selectAll("text.points-labels")
                                .data(scope.items)
                                .enter()
                                .append("text")
                                .attr("class", "points-labels")
                                .attr("text-anchor", "start")
                                .text(function (d) {
                                    return d.brand;
                                })
                                .attr("x", function(d) {
                                    return width + 12;
                                })
                                .attr("y", function(d, i) {
                                    return y(_.last(d.data).avgPerception) + 3;
                                })
                                .attr("fill", "#343434");
                        }
                    }, 0);
                }
            }
        }
    };
}
exports.dtMultiLineAxisSeries = ['$window', '$timeout', '$filter', dtMultiLineAxisSeries];