'use strict';

var multiLineAxisSeries = module.exports = angular.module('dtcharts.charts.multiLineAxisSeries', []);

require('./dtMultiLineAxisSeries').inject(multiLineAxisSeries);