'use strict';

var multilineSeries = module.exports = angular.module('dtcharts.charts.multilineSeries', []);

require('./dtMultilineSeries').inject(multilineSeries);