'use strict';

var d3 = require('d3');
var _ = require('lodash');

exports.inject = function(percentHorizontalBars) {
    percentHorizontalBars.directive('dtPercentHorizontalBars', exports.dtPercentHorizontalBars);
    return exports.dtPercentHorizontalBars;
};

var dtPercentHorizontalBars = function ($window, $timeout, $filter) {
    function calculatePercent(number, percent) {
        return number * percent / 100;
    }
    return {
        restrict: 'EA',
        replace: true,
        templateUrl: 'percent-horizontal-bars/dt-percent-horizontal-bars.html',
        scope: {
            items: '='
        },
        link: {
            pre: function (scope, ele, attrs) {
                //http://www.ng-newsletter.com/posts/d3-on-angular.html
                var renderTimeout;

                //TODO: to constant
                var defaultBarHeight = 32;
                var defaultBarMargin = 32;
                var defaultBarPadding = 5;
                var linesBottomPaddingPercent = 5.71;
                var barsTopPaddingPercent = 17.1;
                var precisionPixel = 1;
                var startWidthTransition = 0;
                var barsSectionHeightPercent = 70.3;
                var idealRadiusPorportion = 10 /20.7;

                var margin = parseInt(attrs.margin) || defaultBarMargin,
                    barHeight = parseInt(attrs.barHeight) || defaultBarHeight,
                    barPadding = parseInt(attrs.barPadding) || defaultBarPadding;

                var widthPixels = d3.select(ele[0].getElementsByClassName('svg-percent-container-horizontal-bars')[0]).node().offsetWidth;
                var heightPixels = d3.select(ele[0].getElementsByClassName('svg-percent-container-horizontal-bars')[0]).node().offsetHeight;
                var chartMargin = {top: 0, right: 15, bottom: 0, left: 91};
                var percent = d3.format('%');

                scope.barsAdded = false;
                scope.barsSectionTopPadding = 0;

                var color = d3.scale.ordinal()
                    .range(["#81d8eb", "#63a5cb", "#4571ab"])
                    .domain(["posMsg", "neuMsg", "negMsg"]);

                var svg = d3.select(ele.find("svg")[0]);

                var percentHorizontalChart = svg.append("g")
                    .attr("class", "percent-horizontal-chart");

                var chart = percentHorizontalChart.append("g")
                    .attr("class", "chart")
                    .attr("transform", "translate(" + chartMargin.left + "," + chartMargin.top + ")");

                var percentHorizontalBarTooltip = d3.select('.svg-percent-container-horizontal-bars').append("div")
                    .attr("class", "percent-horizontal-bar-tooltip")
                    .style('display', "none");

                $window.onresize = function () {
                    scope.$apply();
                };

                scope.$watch(function () {
                    return angular.element($window)[0].innerWidth || angular.element($window)[0].innerHeight;
                }, function () {
                    $timeout(function () {
                        scope.render();
                    }, 750);    //Same as reset transition
                });

                scope.$watch('items', function(newVals, oldVals) {
                    if (newVals !== oldVals) {
                        $timeout(function () {
                            scope.render();
                        }, 750);    //Same as reset transition
                    }
                }, false);

                scope.render = function () {
                    percentHorizontalChart.selectAll("*").remove();

                    if (renderTimeout) clearTimeout(renderTimeout);

                    renderTimeout = $timeout(function () {

                        widthPixels = d3.select(ele[0].getElementsByClassName('svg-percent-container-horizontal-bars')[0]).node().offsetWidth;
                        heightPixels = d3.select(ele[0].getElementsByClassName('svg-percent-container-horizontal-bars')[0]).node().offsetHeight;
                        chartMargin = {top: 0, right: 15, bottom: calculatePercent(heightPixels, linesBottomPaddingPercent) + precisionPixel, left: 91};
                        var width = widthPixels - chartMargin.left - chartMargin.right,
                            height = heightPixels - chartMargin.top - chartMargin.bottom;

                        var barsSectionHeight = calculatePercent(height, barsSectionHeightPercent);
                        var numberOfBars = scope.items.length;
                        var calculatedBarHeight = ((barsSectionHeight / numberOfBars).toFixed(2) || barHeight) - barPadding;
                        scope.calculatedBarHeight = calculatedBarHeight;
                        var barsSectionTopPadding = calculatePercent(heightPixels, barsTopPaddingPercent);
                        scope.barsSectionTopPadding = barsSectionTopPadding;
                        scope.barPadding = barPadding;
                        scope.proportionalRadius = idealRadiusPorportion * calculatedBarHeight;

                        var xScale = d3.scale.linear()
                            .rangeRound([0, width]);

                        var xAxis = d3.svg.axis()
                            .scale(xScale);
                            //.tickValues([0, 0.25, 0.5, 0.75, 1]);

                        scope.items.forEach(function(d) {
                            var x0 = 0;
                            d.mentions = color.domain().map(function(name) {
                                return {
                                    name: name,
                                    x0: x0,
                                    x1: x0 += +d[name]};
                            });
                            d.mentions.forEach(function(d) {
                                d.x0 /= x0;
                                d.x1 /= x0;
                            });
                        });

                        scope.items.sort(function(a, b) {
                            return b.mentions[0].x1 - a.mentions[0].x1;
                        });


                        chart = percentHorizontalChart.append("g")
                            .attr("class", "chart")
                            .attr("transform", "translate(" + chartMargin.left + "," + chartMargin.top + ")");

                        scope.drawedData =_.filter(scope.items, function (item) {
                            return item.negMsg !== 0 && item.neuMsg !== 0 && item.posMsg !== 0;
                        });

                        addXScale();
                        addLineTicks();
                        addBars();
                        addBarsLabels();

                        function addBars() {
                            var brandBar = chart.selectAll(".brand-bar")
                                .data(scope.drawedData)
                                .enter().append("g")
                                .attr("class", function (d) {
                                    return "brand-bar " + d.brand;
                                })
                                .attr("transform", function(d, i) {
                                    var x = 0, y = barsSectionTopPadding + i * (calculatedBarHeight + barPadding);
                                    return "translate(" + x + ", " + y + ")";
                                });

                            brandBar.selectAll("path.percent-horizontal-bars")
                                .data(function(d) {
                                    return _.map(d.mentions, function (mention) { return _.assign(mention, {brand: d.brand})})
                                })
                                .enter()
                                .append("path")
                                .attr("class", function (d) {
                                    return "percent-horizontal-bars " + d.brand;
                                })
                                .attr("d", function(d, i) {
                                    var y = 0, radius = 0;
                                    if (d.x0 != d.x1) {
                                        if (d.x0 === 0 && d.x1 !== 1) {
                                            radius = scope.proportionalRadius;
                                            return leftRoundedRect(xScale(d.x0), 0, xScale(d.x1) - xScale(d.x0), calculatedBarHeight, radius);
                                        }
                                        else if (d.x1 === 1 && d.x0 !== 0) {
                                            radius = scope.proportionalRadius;
                                            return rightRoundedRect(xScale(d.x0), 0, xScale(d.x1) - xScale(d.x0), calculatedBarHeight, radius);
                                        } else if (d.x0 === 0 && d.x1 === 1) {
                                            radius = scope.proportionalRadius;
                                            return leftAndRightRoundedRect(xScale(d.x0), 0, xScale(d.x1) - xScale(d.x0), calculatedBarHeight, radius);
                                        } else {
                                            return rightRoundedRect(xScale(d.x1), 0, xScale(d.x0) - xScale(d.x1), calculatedBarHeight, 0);
                                        }
                                    }
                                })
                                .attr("fill", function(d) {
                                    return color(d.name);
                                })
                                .style('fill-opacity', ".1")
                                .attr('cursor','pointer')
                                .on("mouseover",function (d, i) {
                                    d3.select(this).style('fill-opacity', .2);
                                })
                                .on("mouseout", function(d, i) {
                                    d3.selectAll('path.' + d.brand).style('fill-opacity', function () {
                                        return 1;
                                    });
                                    percentHorizontalBarTooltip.classed("hidden", true)
                                })
                                .on("mousemove", function(d, i) {
                                    updateToolTip(d);
                                })
                                .transition()
                                .duration(750)
                                .style('fill-opacity', "1");
                            scope.barsAdded = true;
                        }

                        function rightRoundedRect(x, y, width, height, radius) {
                            return "M" + x + "," + y
                                + "h" + (width - radius)
                                + "a" + radius + "," + radius + " 0 0 1 " + radius + "," + radius
                                + "v" + (height - 2 * radius)
                                + "a" + radius + "," + radius + " 0 0 1 " + -radius + "," + radius
                                + "h" + (radius - width)
                                + "z";
                        }

                        function leftRoundedRect(x, y, width, height, radius) {
                            return "M" + (x + width) + "," + y
                                + "v" + (height)
                                + "h" + (-width + radius)
                                + "a" + radius + "," + radius + " 0 0 1 -" + radius + ",-" + radius
                                + "v" + (-height + 2 * radius)
                                + "a" + radius + "," + radius + " 0 0 1 " + radius + "," + -radius
                                + "z";
                        }

                        function leftAndRightRoundedRect(x, y, width, height, radius) {
                            return "M" + (x + width - radius) + "," + y
                                + "a" + radius + "," + radius + " 0 0 1 " + radius + "," + radius
                                + "v" + (height - 2 * radius)
                                + "a" + radius + "," + radius + " 0 0 1 " + -radius + "," + radius
                                + "h" + (-width + 2 * radius)
                                + "a" + radius + "," + radius + " 0 0 1 -" + radius + ",-" + radius
                                + "v" + (-height + 2 * radius)
                                + "a" + radius + "," + radius + " 0 0 1 " + radius + "," + -radius
                                + "z";
                        }

                        function addBarsLabels() {
                            //http://alignedleft.com/tutorials/d3/making-a-bar-chart
                            chart.selectAll("text.label")
                                .data(scope.drawedData)
                                .enter()
                                .append("text")
                                .attr("class", "label")
                                .attr("text-anchor", "end")
                                .text(function (d) {
                                    return d.volume;
                                })
                                .attr("x", function(d) {
                                    var barRightPaddig = 10;
                                    return xScale(d.volume) - barRightPaddig;
                                })
                                .attr("y", function(d, i) {
                                    var barsSectionHeight = calculatePercent(height, barsSectionHeightPercent);
                                    var numberOfBars = scope.items.length;
                                    var barsSectionTopPadding = calculatePercent(heightPixels, barsTopPaddingPercent);
                                    var calculatedBarHeight = ((barsSectionHeight / numberOfBars).toFixed(2) || barHeight) - barPadding;
                                    var halfTextHeight = 4;
                                    return barsSectionTopPadding + (calculatedBarHeight / 2) + halfTextHeight + i * (calculatedBarHeight + barPadding);
                                })
                                .attr("font-family", "sans-serif")
                                .attr("font-size", "11px")
                                .attr("fill", "white");
                        }

                        function addLineTicks() {
                            var numberOfTicks = 4;
                            chart.selectAll("line.grid")
                                .data(xScale
                                    .ticks(4))
                                .enter().append("line")
                                .attr("class", "grid")
                                .attr("y1", height)
                                .attr("y2", function (d, i) {
                                    return i === 0 || i === numberOfTicks + 1 ? height - calculatePercent(height, 6.06) : 0;
                                })
                                .attr("x1", xScale)
                                .attr("x2", xScale);
                        }

                        function addXScale () {
                            chart.append("g")
                                .attr("class", "x-axis")
                                .attr("transform", "translate(0," + height + ")")
                                .call(xAxis.ticks(4).tickFormat(percent))
                        }

                        function updateToolTip(d) {
                            d3.event.preventDefault();
                            var mouse = d3.mouse(svg.node()).map( function(d) { return parseInt(d); } );
                            //FIXME: refactor to directive
                            percentHorizontalBarTooltip
                                .classed("hidden", false)
                                .attr("style", "left:"+(mouse[0]- 55)+"px;top:"+ (mouse[1]) +"px")
                                .html("<p style='width: 50px; height: 28px; margin: 0px; font-size: 10px;'> <strong style='font-size: 14px;'>" + $filter('number')(((d.x1 - d.x0) * 100).toFixed(2)) + "%</strong> del total</p>");        //Get CCAA by norm_code
                        }
                    }, 0);
                }
            }
        }
    };
}
exports.dtPercentHorizontalBars = ['$window', '$timeout', '$filter', dtPercentHorizontalBars];