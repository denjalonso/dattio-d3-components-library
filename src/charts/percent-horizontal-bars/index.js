'use strict';

var percentHorizontalBars = module.exports = angular.module('dtcharts.charts.percentHorizontalBars', []);

require('./dtPercentHorizontalBars').inject(percentHorizontalBars);