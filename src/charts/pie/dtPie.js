'use strict';

var d3 = require('d3');
var _ = require('lodash');

exports.inject = function(pie) {
    pie.directive('dtPie', exports.dtPie);
    return exports.dtPie;
};

var dtPie = function ($window, $timeout, $filter) {
    return {
        restrict: 'EA',
        replace: true,
        templateUrl: 'pie/dt-pie.html',
        scope: {
            items: '='
        },
        link: {
            pre: function (scope, ele) {
                //http://bl.ocks.org/mbostock/3887235
                //http://bl.ocks.org/mbostock/3887193
                var renderTimeout;

                var width = d3.select(ele[0].getElementsByClassName('svg-container-pie')[0]).node().offsetWidth,
                    height = d3.select(ele[0].getElementsByClassName('svg-container-pie')[0]).node().offsetHeight,
                    radius = Math.min(width, height) / 2;

                var color = d3.scale.ordinal()
                    .range(["#81d8eb", "#4571ab", "#63a5cb"]);

                var labelCirclesOuterRadius = 9;

                var arc = d3.svg.arc()
                    .outerRadius(radius - labelCirclesOuterRadius)
                    .innerRadius(radius - 40);

                var pie = d3.layout.pie()
                    .sort(null)
                    .value(function(d) {
                        return d.mentions;
                    });

                var pieCenterPosition = (width - (radius + labelCirclesOuterRadius));
                var svg = d3.select(ele.find("svg")[0])
                    .append("g")
                    .attr('class', 'pie-chart')
                    .attr("transform", "translate(" + pieCenterPosition + "," + height / 2 + ")");

                var perceptionPieTooltip = d3.select('.svg-container-pie').append("div")
                    .attr("class", "perception-pie-tooltip")
                    .style('display', "none");

                $window.onresize = function () {
                    scope.$apply();
                };

                scope.$watch(function () {
                    return angular.element($window)[0].innerWidth;
                }, function () {
                    scope.render();
                });

                scope.$watch(function () {
                    return angular.element($window)[0].innerHeight;
                }, function () {
                    scope.render();
                });

                scope.$watch("items", function () {
                    $timeout(function () {
                        scope.render();
                    }, 750);    //Same as reset transition
                });

                scope.render = function () {
                    svg.selectAll("*").remove();

                    if (_.isEmpty(scope.items)) {
                        return;
                    }

                    if (renderTimeout) clearTimeout(renderTimeout);

                    renderTimeout = $timeout(function () {

                        width = d3.select(ele[0].getElementsByClassName('svg-container-pie')[0]).node().offsetWidth;
                        height = d3.select(ele[0].getElementsByClassName('svg-container-pie')[0]).node().offsetHeight;

                        scope.data = [{ section: 'positive', mentions: scope.items.nPos }, { section: 'negative', mentions: scope.items.nNeg }, { section: 'neutral', mentions:scope.items.nNeu}];
                        var totalPercent = _.sum(_.pluck(scope.data, 'mentions'));

                        addPercenText();

                        pieCenterPosition = (width - (radius + labelCirclesOuterRadius));
                        d3.select(".pie-chart")
                            .attr("transform", "translate(" + pieCenterPosition + "," + height / 2 + ")");

                        var g = svg.selectAll(".arc")
                            .data(pie(scope.data))
                            .enter().append("g")
                            .attr("class", "arc")
                            .attr('cursor','pointer')
                            .on("mouseover",function(d,i) {
                                d3.select(this).style('fill-opacity', .2);
                                d3.selectAll('foreignObject').style('color', function (dColor) {
                                    return dColor.data.section !== d.data.section ? "#333333" : "#abb8c0";
                                });

                            })
                            .on("mouseout", function(d,i) {
                                d3.selectAll('.arc').style('fill-opacity', function () {
                                    return 1;
                                });
                                d3.selectAll('foreignObject').style('color', function () {
                                    return "#333333";
                                });
                                perceptionPieTooltip.classed("hidden", true)
                            })
                            .on("mousemove", function(d,i) {
                                updateToolTip(d);
                            });

                        function updateToolTip(d) {
                            d3.event.preventDefault();
                            var pie = d3.select('.svg-container-pie');
                            var mouse = d3.mouse(pie.node()).map( function(d) { return parseInt(d); } );
                            //FIXME: refactor to directive
                            perceptionPieTooltip
                                .classed("hidden", false)
                                .attr("style", "left:"+(mouse[0] - 120)+"px;top:"+ (mouse[1]) +"px")
                                .html("<p style='width: 70px; margin: 0px;'> <strong>" + $filter('number')(d.data.mentions) + "</strong> menciones</p>");        //Get CCAA by norm_code
                        }

                        g.append("path")
                            .attr("d", arc)
                            .style("fill", function(d) {
                                return color(d.data.section);
                            })
                            .transition()
                            .duration(750)
                            .attrTween('d', tweenPie);;

                        addCirclesLabels();
                        addLines();

                        var xaux, yaux;
                        function addCirclesLabels() {
                            var labelr = radius - labelCirclesOuterRadius;
                            g.append("circle")
                                .attr("class", "pie-perception-labels")
                                .attr("transform", function (d) {
                                    var c = arc.centroid(d),
                                        x = c[0],
                                        y = c[1],
                                    // pythagorean theorem for hypotenuse
                                        h = Math.sqrt(x * x + y * y);
                                    return "translate(" + (x / h * labelr) + ',' +
                                        (y / h * labelr) + ")";
                                })
                                .attr('fill', 'white')
                                .attr('stroke', '#f1f1f1')
                                .attr('stroke-width', '1')
                                .attr('r', '9')
                                .style("display", function (d) {
                                    if (d.value === 0) {
                                        return "none";
                                    } else {
                                        return null;
                                    }
                                });

                            g.append("svg:foreignObject")
                                .attr("width", 9)
                                .attr("height", 9)
                                .attr("y", function (d) {
                                    if (d.data.section !== "neutral") {
                                        return "-5px";
                                    } else {
                                        return "-4px";
                                    }

                                })
                                .attr("x", "-4px")
                                .style("font-size", "9px")
                                .attr("transform", function (d) {
                                    var c = arc.centroid(d),
                                        x = c[0],
                                        y = c[1],
                                    // pythagorean theorem for hypotenuse
                                        h = Math.sqrt(x * x + y * y);
                                    xaux = x;
                                    yaux = y;
                                    if (d.data.section !== "neutral") {
                                        return "translate(" + (x / h * labelr) + ',' +
                                            (y / h * labelr) + ")";
                                    } else {
                                        return "translate(" + (x / h * labelr) + ',' +
                                            (y / h * labelr) + ") rotate(90)";
                                    }
                                })
                                .append("xhtml:span")
                                .attr("class", function (d) {
                                    if (d.data.section === "positive") {
                                        return "control glyphicon glyphicon-plus"
                                    }
                                    else if (d.data.section === "negative") {
                                        return "control glyphicon glyphicon-minus"
                                    }
                                    else {
                                        return "control glyphicon glyphicon-pause"
                                    }
                                })
                                .style("display", function (d) {
                                    if (d.value === 0) {
                                        return "none";
                                    } else {
                                        return null;
                                    }
                                });
                        }

                        function addPercenText() {
                            d3.select(ele.find("svg")[0])
                                .selectAll(".percent-text").remove();
                            d3.select(ele.find("svg")[0])
                                .selectAll(".percent-text")
                                .data(scope.data)
                                .enter().append("g")
                                .attr("class", function (d) {
                                    if (d.section === 'positive') {
                                        return "percent-text positive";
                                    } else if (d.section === 'negative') {
                                        return "percent-text negative";
                                    } else {
                                        return "percent-text neutral";
                                    }
                                })
                                .append('text')
                                .attr("x", "0")
                                .attr("y", function (d) {
                                    if (d.section === 'positive') {
                                        return "28.17";
                                    } else if (d.section === 'negative') {
                                        return "80";
                                    } else {
                                        return "134.7";
                                    }
                                })
                                .style("fill", "#333333")
                                .style("font-weight", "bold")
                                .text(function (d) {
                                    return (d.mentions * 100 / totalPercent).toFixed(2) + "%";
                                });

                            d3.select(ele.find("svg")[0])
                                .selectAll(".section-percent-text")
                                .data(scope.data)
                                .enter().append("g")
                                .attr("class", "section-percent-text")
                                .append('text')
                                .attr("x", "0")
                                .attr("y", function (d) {
                                    if (d.section === 'positive') {
                                        return "40.17";
                                    } else if (d.section === 'negative') {
                                        return "92";
                                    } else {
                                        return "146.7";
                                    }
                                })
                                .style("fill", "#333333")
                                .text(function (d) {
                                    if (d.section === 'positive') {
                                        return "positivo";
                                    } else if (d.section === 'negative') {
                                        return "negativo";
                                    } else {
                                        return "neutro";
                                    }
                                });
                        }

                        function addLines () {
                            var labelr = radius - labelCirclesOuterRadius;
                            var x = xaux,
                                y = yaux,
                            // pythagorean theorem for hypotenuse
                                h = Math.sqrt(x * x + y * y);

                            var lineYaxisPosition = 80 - 53.3;
                            var distanceSquareBorderToPieBorder = h - Math.sqrt((h * h) - (lineYaxisPosition * lineYaxisPosition));

                            if (!_.isNaN(distanceSquareBorderToPieBorder)) {
                                d3.selectAll('line.pie-lines').remove();
                                var line1 = d3.select('svg.pie').append("line")          // attach a line
                                    .attr("class", "pie-lines")
                                    .style("stroke", "white")  // colour the line
                                    .attr("x1", 0)     // x position of the first end of the line
                                    .attr("y1", 53.3)      // y position of the first end of the line
                                    .attr("x2", width - ((radius  * 2)) + distanceSquareBorderToPieBorder - 2)     // x position of the second end of the line
                                    .transition()
                                    .duration(750)
                                    .attr("y2", 53.3)
                                    .style("stroke", "#ebebeb")
                                    .style("stroke-width", "2");

                                d3.select('svg.pie').append("line")          // attach a line
                                    .attr("class", "pie-lines")
                                    .style("stroke", "white")  // colour the line
                                    .attr("x1", 0)     // x position of the first end of the line
                                    .attr("y1", 106.9)      // y position of the first end of the line
                                    .transition()
                                    .duration(750)
                                    .attr("x2", width - ((radius  * 2)) + distanceSquareBorderToPieBorder - 2)     // x position of the second end of the line
                                    .attr("y2", 106.9)
                                    .style("stroke", "#ebebeb")
                                    .style("stroke-width", "2");


                            }
                        }

                        function tweenPie(finish) {
                            var start = {
                                startAngle: 0,
                                endAngle: 0
                            };
                            var i = d3.interpolate(start, finish);
                            return function(d) { return arc(i(d)); };
                        }
                    }, 0);
                }
            }
        }
    };
}
exports.dtPie = ['$window', '$timeout', '$filter', dtPie];