'use strict';

var pie = module.exports = angular.module('dtcharts.charts.pie', []);

require('./dtPie').inject(pie);