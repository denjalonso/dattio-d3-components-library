exports.inject = function (progressBar) {
    progressBar.directive("dtProgressBar", exports.dtProgressBar);
    return exports.dtProgressBar;
};

exports.dtProgressBar = ["$window", "$timeout", function ($window, $timeout) {
    function setProgressCompleted(scope, progressCompleted) {
        scope.progressCompleted = {
            width: progressCompleted + "%"
        };
    }
    return {
        restrict: "E",
        replace: true,
        templateUrl: "progress-bar/progress-bar.html",
        scope: {
            percent: "="
        },
        link: function (scope) {
            scope.setProgressCompleted = setProgressCompleted.bind(null, scope);
            scope.setProgressCompleted(scope.percent);
            scope.$watch("percent", function (newValue, oldValue) {
                if (newValue != oldValue) {
                    scope.setProgressCompleted(newValue);
                }
            });
        }
    }
}];