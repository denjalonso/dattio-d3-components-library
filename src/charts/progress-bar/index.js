"use strict";

var progressBar = module.exports = angular.module("dtcharts.charts.progressBar", []);

require("./dtProgressBar").inject(progressBar);