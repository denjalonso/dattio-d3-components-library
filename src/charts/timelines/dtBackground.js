var d3 = require('d3');
var _ = require('lodash');

exports.inject = function (dtBackground) {
    dtBackground.directive('dtBackground', exports.dtBackground);
    return exports.dtBackground;
};

exports.dtBackground = ['$window', '$timeout', function ($window, $timeout) {
    return {
        restrict: 'EA',
        templateNamespace: 'svg',
        replace: true,
        scope: {
            addToG: '@',
            parentSvg: '@'
        },
        link: function link(scope) {
            var renderTimeout;
            var svg = d3.select(scope.parentSvg);
            var g = d3.select(scope.addToG);

            var parentWidth = svg.node().offsetWidth,
                parentHeight = svg.node().offsetHeight;

            $window.onresize = function () {
                scope.$apply();
            };

            scope.$watch(function () {
                return angular.element($window)[0].innerWidth || angular.element($window)[0].innerHeight;
            }, function () {
                $timeout(function () {
                    scope.render();
                }, 750);    //Same as reset transition
            });

            scope.render = function () {
                g.selectAll('*').remove();

                if (renderTimeout) clearTimeout(renderTimeout);

                renderTimeout = $timeout(function () {
                    parentWidth = svg.node().offsetWidth === undefined ?  jQuery(scope.parentSvg).width() : svg.node().offsetWidth,
                    parentHeight = svg.node().offsetHeight === undefined ? jQuery(scope.parentSvg).height() : svg.node().offsetHeight,

                    addBackgroundRect();

                    function addBackgroundRect() {
                        if (g.select('rect.dt-background').empty()) {
                            g.append("rect")
                                .attr("class", "dt-background")
                                .attr("width", parentWidth)
                                .attr("height", parentHeight);
                        }
                    }
                }, 0);
            };
        }
    };
}];
