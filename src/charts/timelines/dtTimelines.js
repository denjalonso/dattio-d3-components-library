'use strict';

var d3 = require('d3');
var _ = require('lodash');
var moment = require('moment');

exports.inject = function(timelines) {
    timelines.directive('dtTimelines', exports.dtTimelines);
    return exports.dtTimelines;
};

var dtTimelines = function ($window, $timeout, $filter, componentsBBoxMargin) {
    function processTimeLineSeriesData(items, brands) {
        return _.map(brands, function (brand) {
            return {
                brand: brand.brand,
                data: _.map(_.where(items, { 'brand': brand.brand}), function (item) {
                    return {
                        brand: brand.brand,
                        date: item.timestamp_ms,
                        volume: item.volume,
                        eventId: item.eventId
                    }
                })
            }
        });
    }
    return {
        restrict: 'EA',
        replace: true,
        templateUrl: 'timelines/timelines.html',
        scope: {
            brands: '=',
            items: '=',
            defaultEventSelected: '='
        },
        link: {
            pre: function (scope, ele, attrs) {
                var renderTimeout;

                var minVolumenMentions, maxVolumenMentions;

                scope.componentsBBoxMargin = componentsBBoxMargin;
                scope.legendOblong = true;

                var d3ParseDate = d3.time.format("%d/%m");

                var widthPixels = d3.select(ele[0].getElementsByClassName('svg-timelines')[0]).node().offsetWidth;
                var heightPixels = d3.select(ele[0].getElementsByClassName('svg-timelines')[0]).node().offsetHeight;
                var chartMargin = {
                    top: 150,
                    right: 50,
                    bottom: 82,
                    left: 50
                };

                var svg = d3.select(ele.find("svg")[0]);

                var timelines = svg.append("g")
                    .attr("class", "timelines-chart");

                var chart = timelines.append("g")
                    .attr("class", "timelines-value")
                    .attr("transform", "translate(" + chartMargin.left + "," + chartMargin.top + ")");

                var timelinesTooltip = d3.select('.svg-timelines').append("div")
                    .attr("class", "timelines-tooltip")
                    .style('display', "none");

                $window.onresize = function () {
                    scope.$apply();
                };

                scope.$watch(function () {
                    return angular.element($window)[0].innerWidth || angular.element($window)[0].innerHeight;
                }, function () {
                    $timeout(function () {
                        scope.render();
                    }, 750);    //Same as reset transition
                });

                scope.$watch('items', function(newVals, oldVals) {
                    if (newVals !== oldVals) {
                        $timeout(function () {
                            scope.render();
                        }, 750);    //Same as reset transition
                    }
                }, false);

                scope.render = function () {
                    timelines.selectAll("*").remove();

                    if (renderTimeout) clearTimeout(renderTimeout);

                    renderTimeout = $timeout(function () {
                        scope.selectedEvent = scope.defaultEventSelected;

                        widthPixels = d3.select(ele[0].getElementsByClassName('svg-timelines')[0]).node().offsetWidth;
                        heightPixels = d3.select(ele[0].getElementsByClassName('svg-timelines')[0]).node().offsetHeight;
                        chartMargin = {
                            top: 150,
                            right: 50,
                            bottom: 82,
                            left: 50
                        };

                        var width = widthPixels - chartMargin.left - chartMargin.right,
                            height = heightPixels - chartMargin.top - chartMargin.bottom;

                        var xScaleDomainValues = d3.extent(scope.items, function (d) { return d.timestamp_ms; });
                        var xScaleDomain = [new Date(xScaleDomainValues[0]), new Date(xScaleDomainValues[1])];
                        var x = d3.time.scale()
                            .range([0, width])
                            .domain(xScaleDomain).nice(d3.time.day);

                        var y = d3.scale.ordinal()
                            .rangeRoundBands([height, 0]);

                        y.domain(scope.items.map(function(d) {
                            return d.brand;
                        }));

                        //8 is the stroke line width
                        var radius = d3.scale.sqrt();

                        if (_.size(scope.items) === 1) {
                            radius.domain([minVolumenMentions, maxVolumenMentions]).range([8, 25]);           //FIXME: set range to max area posible of small region
                        } else {
                            minVolumenMentions = _.min(_.pluck(scope.items, 'volume'));
                            maxVolumenMentions = _.max(_.pluck(scope.items, 'volume'));
                            radius.domain([minVolumenMentions, maxVolumenMentions]).range([8, 25]);           //FIXME: set range to max area posible of small region
                        }

                        scope.timeLineSeries = processTimeLineSeriesData(scope.items, scope.brands);

                        var moment = require('moment');
                        var xScaleInitDate = new Date(xScaleDomainValues[0]);
                        var initDate = moment([xScaleInitDate.getFullYear(), xScaleInitDate.getMonth(), xScaleInitDate.getDate()]).toDate();
                        var xScaleFinalDate = new Date(xScaleDomainValues[1]);
                        var finalDate = moment([xScaleFinalDate.getFullYear(), xScaleFinalDate.getMonth(), xScaleFinalDate.getDate(), 23, 59, 59]).toDate();
                        scope.drawedDataLines = _.map(scope.brands, function (brand) {
                            return [
                                {
                                    date: initDate,
                                    brand: brand.brand
                                },
                                {
                                    date: finalDate,
                                    brand:brand.brand
                                }];
                        });
                        scope.drawedDataPoints = _.pluck(scope.timeLineSeries, 'data');

                        chart = timelines.append("g")
                            .attr("class", "timelines-value")
                            .attr("transform", "translate(" + chartMargin.left + "," + chartMargin.top + ")");

                        addXAxis();
                        addTimelineSeries();
                        addWhiteBackGroundPoints();
                        addPoints();

                        function addXAxis() {
                            var tickSize = 12;
                            var xAxis = d3.svg.axis()
                                .scale(x)
                                .orient("bottom")
                                .ticks(d3.time.day, 1)
                                .tickFormat(d3ParseDate)
                                .tickPadding(7)
                                .innerTickSize(tickSize)
                                .outerTickSize(0);

                            chart.append("g")
                                .attr("class", "x-axis")
                                .attr("transform", "translate(0," + height + ")")
                                .call(xAxis)
                                .selectAll("line")
                                .attr("transform", "translate(0, -" + tickSize + ")");
                        }
                        function addTimelineSeries() {
                            var line = d3.svg.line()
                                .interpolate("cardinal")
                                .x(function(d) {
                                    return x(d.date);
                                })
                                .y(function(d) {
                                    return y(d.brand);
                                });

                            chart.selectAll('.time-line-serie')
                                .data(scope.drawedDataLines)
                                .enter()
                                .append("path")
                                .attr("class", "time-line-serie")
                                .attr('stroke', "white")
                                .style("stroke-width", 8)
                                .attr("d", line);
                        }
                        function addWhiteBackGroundPoints() {
                            var volumeBalls = chart.selectAll('.volume-white-bg-balls')
                                .data(scope.drawedDataPoints)
                                .enter()
                                .append("g")
                                .attr("class", "volume-white-bg-balls");

                            volumeBalls.selectAll('.volume-white-bg-balls')
                                .data(function(d, index){
                                    var a = [];
                                    d.forEach(function(point,i){
                                        a.push({'index': index, 'point': point});
                                    });
                                    return a;
                                })
                                .enter()
                                .append('circle')
                                .attr('class', "timeline-white-bg-balls")
                                .attr('stroke', "white")
                                .style("stroke-width", 4)
                                .attr("r", function (d, i) {
                                    return radius(d.point.volume);
                                })
                                .attr('fill', "white")
                                .attr("transform", function(d) {
                                    return "translate(" + x(d.point.date) + "," + y(d.point.brand) + ")";
                                });
                        }
                        function addPoints() {
                            var volumeBalls = chart.selectAll('.volume-balls')
                                .data(scope.drawedDataPoints)
                                .enter()
                                .append("g")
                                .attr("class", "volume-balls");

                            volumeBalls.selectAll('.volume-balls')
                                .data(function(d, index){
                                    var a = [];
                                    d.forEach(function(point,i){
                                        a.push({'index': index, 'point': point});
                                    });
                                    return a;
                                })
                                .enter()
                                .append('circle')
                                .attr('class',function (d) {
                                    return 'timeline-balls' + (d.point.eventId === scope.selectedEvent ? ' selected' : '');
                                })
                                .attr('stroke', "white")
                                .style("stroke-width", 4)
                                .attr("r", function (d, i) {
                                    return radius(d.point.volume);
                                })
                                .attr('fill', function(d,i){
                                    return _.first(_.where(scope.brands, {brand: d.point.brand})).color;
                                })
                                .attr("transform", function(d) {
                                    return "translate(" + x(d.point.date) + "," + y(d.point.brand) + ")";
                                })
                                .on("mouseover",function (d, i) {
                                    //d3.select(this).style('fill-opacity', .2);
                                    //d3.select(this).style('stroke-opacity', .2);
                                })
                                .on("mouseout", function(d, i) {
                                    //d3.selectAll('circle.timeline-balls').style('fill-opacity', function () {
                                    //    return 1;
                                    //});
                                    //d3.selectAll('circle.timeline-balls').style('stroke-opacity', function () {
                                    //    return 1;
                                    //});
                                    timelinesTooltip.classed("hidden", true);
                                })
                                .on("mousemove", function(d, i) {
                                    updateToolTip(d);
                                })
                                .on("click", function (d) {
                                    d3.selectAll('circle.timeline-balls').classed("selected", false);
                                    d3.select(this).classed("selected", true);
                                    changeSelectedMilestone(d.point.eventId);
                                });
                        }
                        function updateToolTip(d) {
                            d3.event.preventDefault();
                            var mouse = d3.mouse(svg.node()).map( function(d) { return parseInt(d); } );
                            //FIXME: refactor to directive
                            timelinesTooltip
                                .classed("hidden", false)
                                .attr("style", "left:"+(mouse[0]- 100)+"px;top:"+ (mouse[1]) +"px")
                                .html(
                                "<div style='width: 100%;'>" +
                                "<div style='position: relative; padding: 10px; border-bottom: 1px solid #e9e9e9; font-size: 10px;'>" +
                                "<div style='height: 12px; width: 12px; border-radius: 6px; background-color:" + (_.first(_.where(scope.brands, {brand: d.point.brand})).color) + "'></div>" +
                                "<span style='position: absolute; top: 10px; left: 28px;'>" + d.point.brand + "</span>" +
                                "</div>" +
                                "<p style='padding: 10px; width: 90px; height: 53px; margin: 0px; font-size: 12px;'> " +
                                "<strong style='font-size: 14px;'>" + $filter('number')(d.point.volume) + "</strong> " +
                                "menciones" +
                                "</p>" +
                                "</div>"
                            );
                        }
                        function changeSelectedMilestone(selectedEventId) {
                            scope.selectedEvent = selectedEventId;
                            scope.$emit('milestones:change-selected', selectedEventId);
                        }
                    }, 0);
                }
            }
        }
    };
}
exports.dtTimelines = ['$window', '$timeout', '$filter', 'componentsBBoxMargin', dtTimelines];