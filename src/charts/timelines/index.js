'use strict';

var timelines = module.exports = angular.module('dtcharts.charts.timelines', []);

require('./dtTimelines').inject(timelines);
var dtBackground = require('./dtBackground');
dtBackground.$inject = ['$window', '$timeout'];
dtBackground.inject(timelines);