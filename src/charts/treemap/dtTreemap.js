"use strict";

var d3 = require('d3');
var _ = require('lodash');

exports.inject = function (treemap) {
    treemap.directive("dtTreemap", exports.dtTreemap);
    return exports.dtTreemap;
};

var dtTreemap = function ($window, $timeout, $filter) {
    function isNodeInTopLeftTreemap(x, y) {
        return x === 0 && y === 0;
    }

    function isNodeInTopRightTreemap(x, y, width, nodeWidth) {
        return x === (width - nodeWidth) && y === 0;
    }

    function isNodeInBottomLeftTreemap(x, y, height, nodeHeight) {
        return x === 0 && y === (height - nodeHeight);
    }

    function isNodeInBottomRightTreemap(x, y, nodeWidth, nodeHeight, width, height) {
        return width === (x + nodeWidth) && height === (y + nodeHeight);
    }

    function makeNodeClass(d, width, height) {
        var borderRadiusClass = "";
        if (isNodeInTopLeftTreemap(d.x, d.y)) {
            borderRadiusClass += "top-left-border ";
        }
        if (isNodeInTopRightTreemap(d.x, d.y, width, Math.max(0, d.dx)))
        {
            borderRadiusClass += "top-right-border ";
        }
        if (isNodeInBottomLeftTreemap(d.x, d.y, height, Math.max(0, d.dy))) {
            borderRadiusClass += "bottom-left-border ";
        }
        if (isNodeInBottomRightTreemap(d.x, d.y, Math.max(0, d.dx), Math.max(0, d.dy), width, height)) {
            borderRadiusClass += "bottom-right-border ";
        }
        return "node " + borderRadiusClass;
    }

    return {
        restrict: 'EA',
        replace: true,
        templateUrl: 'treemap/dt-treemap.html',
        scope: {
            items: '='
        },
        link: {
            pre: function (scope, ele, attrs) {
                var renderTimeout;
                var widthPixels = d3.select(ele[0].getElementsByClassName('svg-treemap')[0]).node().offsetWidth;
                var heightPixels = d3.select(ele[0].getElementsByClassName('svg-treemap')[0]).node().offsetHeight;
                var chartMargin = {
                    top: 0,
                    right: 0,
                    bottom: 0,
                    left: 0
                };

                var color = d3.scale.linear()
                    .range(['#81d8eb', '#4571ab']);

                var treemapContainer = d3.select(".svg-treemap");

                var treemapToolIp = d3.select('.dt-treemap').append("div")
                    .attr("class", "treemap-tooltip")
                    .style('display', "none");

                $window.onresize = function () {
                    scope.$apply();
                };

                scope.$watch(function () {
                    return angular.element($window)[0].innerWidth || angular.element($window)[0].innerHeight;
                }, function () {
                    $timeout(function () {
                        if (scope.items) {
                            scope.render();
                        }
                    }, 750);    //Same as reset transition
                });

                scope.$watch('items', function(newVals, oldVals) {
                    if (newVals !== oldVals) {
                        $timeout(function () {
                            scope.render();
                        }, 750);    //Same as reset transition
                    }
                }, false);

                scope.render = function () {
                    treemapContainer.selectAll("*").remove();

                    if (renderTimeout) clearTimeout(renderTimeout);

                    renderTimeout = $timeout(function () {
                        widthPixels = d3.select(ele[0].getElementsByClassName('svg-treemap')[0]).node().offsetWidth;
                        heightPixels = d3.select(ele[0].getElementsByClassName('svg-treemap')[0]).node().offsetHeight;
                        chartMargin = {
                            top: 0,
                            right: 0,
                            bottom: 0,
                            left: 0
                        };

                        var width = widthPixels - chartMargin.left - chartMargin.right,
                            height = heightPixels - chartMargin.top - chartMargin.bottom;

                        console.log("pinta: ");
                        console.log(scope.items);
                        treemapContainer.append("div")
                            .style("position", "relative")
                            .style("width", (widthPixels + chartMargin.left + chartMargin.right) + "px")
                            .style("height", (heightPixels + chartMargin.top + chartMargin.bottom) + "px")
                            .style("left", chartMargin.left + "px")
                            .style("top", chartMargin.top + "px");

                        var minDataRange = _.min(_.pluck(scope.items.children, "freq"));
                        color.domain([minDataRange, _.max(_.pluck(scope.items.children, "freq"))]);

                        var treemap = d3.layout.treemap()
                            .size([width, height])
                            .sticky(true)
                            .value(function(d) { return d.freq; });

                        var node = treemapContainer.datum(scope.items).selectAll(".node")
                            .data(treemap.nodes)
                            .enter()
                            .append("div")
                            .attr("class", function (d) {
                                return makeNodeClass(d, width, height);
                            })
                            .style('cursor','pointer')
                            .call(position)
                            .style("background", "white")
                            .on("mouseover",function (d, i) {
                                d3.select(this).style('opacity', .7);
                            })
                            .on("mouseout", function(d, i) {
                                d3.selectAll('.node').style('opacity', function () {
                                    return 1;
                                });
                                treemapToolIp.classed("hidden", true);
                            })
                            .on("mousemove", function(d, i) {
                                updateToolTip(d);
                            })
                            .append("span")
                            .attr('class', 'label Open-Sans-normal')
                            .text(function(d) { return d.children ? null : $filter('capitalize')(d.word); });

                        d3.selectAll(".node")
                            .transition()
                            .duration(750)
                            .style("background", function(d) { return d.children ? null : color(d.freq); });

                        function position() {
                            this.style("left", function(d) { return d.x + "px"; })
                                .style("top", function(d) { return d.y + "px"; })
                                .style("width", function(d) {
                                    return Math.max(0, d.dx) + "px";
                                })
                                .style("height", function(d) {
                                    return Math.max(0, d.dy) + "px";
                                });
                        }

                        function updateToolTip(d) {
                            d3.event.preventDefault();
                            var mouse = d3.mouse(treemapContainer.node()).map( function(d) { return parseInt(d); } );
                            //FIXME: refactor to directive
                            treemapToolIp
                                .classed("hidden", false)
                                .attr("style", "left:"+(mouse[0]- 55)+"px;top:"+ (mouse[1]) +"px")
                                .html("<p class='Open-Sans-light' style='width: 54px; height: 44px; margin: 0px; font-size: 12px;'> <strong class='Open-Sans-extra-bold' style='font-size: 18px;'>" +
                                $filter('number')((d.freq * 100 / _.sum(_.pluck(scope.items.children, 'freq'))).toFixed(1)) + "%</strong> del total</p>");        //Get CCAA by norm_code
                        }
                    }, 0);
                }
            }
        }
    }
};

exports.dtTreemap = ["$window", "$timeout", "$filter", dtTreemap];