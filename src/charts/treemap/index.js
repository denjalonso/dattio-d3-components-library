"use strict";

var treemap = module.exports = angular.module("dtcharts.charts.treemap", []);

require("./dtTreemap").inject(treemap);