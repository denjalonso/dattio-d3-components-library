'use strict';

var d3 = require('d3');
var _ = require('lodash');

exports.inject = function(verticalBarsMultiDimensions) {
    verticalBarsMultiDimensions.directive('dtVerticalBarsMultiDimensions', exports.dtVerticalBarsMultiDimensions);
    return exports.dtVerticalBarsMultiDimensions;
};

var dtVerticalBarsMultiDimensions = function ($window, $timeout, $filter) {
    function calculatePercent(number, percent) {
        return number * percent / 100;
    }

    function calculateBadgeWidth(proportionalBadgeWidth) {
        var maxBadgeWidth = 37, minBadgeWidth = 23;
        if (proportionalBadgeWidth < maxBadgeWidth && proportionalBadgeWidth > minBadgeWidth) {
            return proportionalBadgeWidth;
        } else if (proportionalBadgeWidth < minBadgeWidth) {
            return minBadgeWidth;
        } else if (proportionalBadgeWidth > maxBadgeWidth) {
            return maxBadgeWidth
        }
    }
    return {
        restrict: 'EA',
        replace: true,
        templateUrl: 'vertical-bars-multi-dimensions/dt-vertical-bars-multi-dimensions.html',
        scope: {
            items: '=',
            brands: '='
        },
        link: {
            //http://bl.ocks.org/mbostock/3886208
            pre: function (scope, ele, attrs) {
                var renderTimeout;

                //TODO: to constant
                var defaultBarWidth = 52;
                var defaultBarMargin = 32;
                var defaultBarPadding = 0;
                var badgeHeight = 42;

                var linesLeftPaddingPercent = 7.78;
                var linesBottomPaddingPercent = 13;
                var precisionPixel = 1;
                var barsSectionWidthPercent = 78.22;
                var idealRadiusPorportion = 10 / 117;
                var idealBadgeWidthProportion = 37 / 74;

                var margin = parseInt(attrs.margin) || defaultBarMargin,
                    barWidth = parseInt(attrs.barHeight) || defaultBarWidth,
                    barPadding = parseInt(attrs.barPadding) || defaultBarPadding;

                var widthPixels = d3.select(ele[0].getElementsByClassName('svg-container-vertical-bars-multi-dimensions')[0]).node().offsetWidth;
                var heightPixels = d3.select(ele[0].getElementsByClassName('svg-container-vertical-bars-multi-dimensions')[0]).node().offsetHeight;
                var chartMargin = {top: 0, right: 15, bottom: 0, left: 38};

                scope.barsAdded = true;

                var color = d3.scale.ordinal()
                    .range(_.pluck(scope.brands, 'color'))
                    .domain(_.pluck(scope.brands, 'brand'));

                var svg = d3.select(ele.find("svg")[0]);

                var verticalChartMultiDimensions = svg.append("g")
                    .attr("class", "vertical-chart-multi-dimensions");

                var chart = verticalChartMultiDimensions.append("g")
                    .attr("class", "chart")
                    .attr("transform", "translate(" + chartMargin.left + "," + chartMargin.top + ")");

                var verticalBarTooltip = d3.select('.svg-container-vertical-bars-multi-dimensions').append("div")
                    .attr("class", "vertical-bar-multi-dimensions-tooltip")
                    .style('display', "none");

                $window.onresize = function () {
                    scope.$apply();
                };

                scope.$watch(function () {
                    return angular.element($window)[0].innerWidth || angular.element($window)[0].innerHeight;
                }, function () {
                    $timeout(function () {
                        scope.render();
                    }, 750);    //Same as reset transition
                });

                scope.$watch('items', function(newVals, oldVals) {
                    if (newVals !== oldVals) {
                        $timeout(function () {
                            scope.render();
                        }, 750);    //Same as reset transition
                    }
                }, false);

                scope.render = function () {
                    verticalChartMultiDimensions.selectAll("*").remove();
                    svg.selectAll(".category-badge-multi-dim").remove();
                    svg.selectAll(".badge-text").remove();

                    if (renderTimeout) clearTimeout(renderTimeout);

                    renderTimeout = $timeout(function () {

                        widthPixels = d3.select(ele[0].getElementsByClassName('svg-container-vertical-bars-multi-dimensions')[0]).node().offsetWidth;
                        heightPixels = d3.select(ele[0].getElementsByClassName('svg-container-vertical-bars-multi-dimensions')[0]).node().offsetHeight;
                        chartMargin = {
                            top: badgeHeight,
                            right: 0,
                            bottom: calculatePercent(heightPixels, linesBottomPaddingPercent) + precisionPixel,
                            left: calculatePercent(widthPixels, linesLeftPaddingPercent) + precisionPixel
                        };
                        var width = widthPixels - chartMargin.left - chartMargin.right,
                            height = heightPixels - chartMargin.top - chartMargin.bottom;

                        var barsSectionWidth = calculatePercent(width, barsSectionWidthPercent);
                        var numberOfBars = scope.items.length;
                        var calculatedBarWidth = ((barsSectionWidth / numberOfBars).toFixed(2) || barWidth) - barPadding;
                        if (defaultBarWidth < calculatedBarWidth) {
                            scope.calculatedBarWidth = defaultBarWidth;
                        }

                        chart = verticalChartMultiDimensions.append("g")
                            .attr("class", "chart")
                            .attr("transform", "translate(" + chartMargin.left + "," + chartMargin.top + ")");

                        scope.barPadding = barPadding;

                        var xScale = d3.scale.ordinal()
                            .rangeRoundBands([0, width], .3, 0.5);

                        var yScale = d3.scale.linear()
                                .range([height, 0]);

                        var xAxis = d3.svg.axis()
                            .scale(xScale)
                            .orient("bottom");

                        var yAxis = d3.svg.axis()
                            .scale(yScale)
                            .orient("left")
                            .ticks(4)
                            .tickFormat(function (d) {
                                return $filter('number')((d/1000).toFixed(2)) + " K";
                            });

                        scope.proportionalRadius = idealRadiusPorportion * xScale.rangeBand();

                        var normalicedData = _.map(scope.items, function (item) {
                            var resultObject = {
                                "category": item.category
                            };
                            _.forEach(item.data, function (brandVolume) {
                                _.assign(resultObject, _.zipObject([_.values(brandVolume)]));
                            });
                            return resultObject;

                        });
                        normalicedData.forEach(function(d) {
                            var y0 = 0;
                            d.volumeMentions = color.domain().map(function(name) {
                                return {
                                    name: name,
                                    y0: y0,
                                    y1: y0 += +d[name]};
                            });
                            d.total = d.volumeMentions[d.volumeMentions.length - 1].y1;
                        });

                        normalicedData.sort(function(a, b) { return b.total - a.total; });
                        xScale.domain(normalicedData.map(function(d) { return d.category; }));
                        yScale.domain([0, d3.max(normalicedData, function(d) { return d.total; })]);

                        addXScale();
                        addYScale();
                        addLineTicks();
                        addBars();
                        addBadges();
                        addBadgesText();

                        function addBadgesText() {
                            var drawedBadgesText =_.filter(normalicedData, function (item) {
                                return item.total !== 0;
                            });
                            svg.selectAll("text.badge-text")
                                .data(drawedBadgesText)
                                .enter()
                                .append("text")
                                .attr("class", "badge-text")
                                .attr("text-anchor", "middle")
                                .text(function (d) {
                                    return $filter('number')((d.total / 1000).toFixed(2)) + "K";
                                })
                                .attr("x", function(d) {
                                    var proportionalBadgeWidth = idealBadgeWidthProportion * xScale.rangeBand();
                                    var badgeWidth = calculateBadgeWidth(proportionalBadgeWidth);
                                    var diference = 37 - badgeWidth;
                                    return (xScale.rangeBand()/2) + chartMargin.left - (12.75 - (diference / 2)) + xScale(d.category) + ((d.total / 1000) > 99 ? 10 - (diference / 2) : 14 - (diference / 2));
                                })
                                .attr("y", function (d) {
                                    var minTopHeight = idealRadiusPorportion * xScale.rangeBand() / 2;
                                    var lastRectangle = _.last(d.volumeMentions);
                                    return ((yScale(lastRectangle.y0) - yScale(lastRectangle.y1)) < minTopHeight ? 24 + yScale(d.total) - (minTopHeight + (idealRadiusPorportion * xScale.rangeBand())) : 16 + yScale(d.total));
                                })
                                .attr("fill", "white");
                        }

                        function addBadges() {
                            var drawedBadges =_.filter(normalicedData, function (item) {
                                return item.total !== 0;
                            });
                            svg.selectAll("path.category-badge-multi-dim")
                                .data(drawedBadges)
                                .enter()
                                .append("path")
                                .attr("class", "category-badge-multi-dim")
                                .attr("d", function(d, i) {
                                    var minTopHeight = idealRadiusPorportion * xScale.rangeBand() / 2;
                                    var lastRectangle = _.last(d.volumeMentions);
                                    var proportionalBadgeWidth = idealBadgeWidthProportion * xScale.rangeBand();
                                    var radius = 12;
                                    var badgeWidth = calculateBadgeWidth(proportionalBadgeWidth);
                                    var diference = 37 - badgeWidth;
                                    return badge((xScale.rangeBand()/2) + chartMargin.left - (12.75 - (diference / 2)) + xScale(d.category), (yScale(lastRectangle.y0) - yScale(lastRectangle.y1)) < minTopHeight ? yScale(d.total) - (minTopHeight + (idealRadiusPorportion * xScale.rangeBand())) + (badgeHeight/2 - 12.75) : yScale(d.total), badgeWidth, radius);
                                })
                                .attr("fill", "black");
                        }

                        function badge (x, y , width, radius) {
                            return "M" + x + "," + y
                                + "h" + (width - radius)
                                + "a" + radius + "," + radius + " 0 0 1 " + radius + "," + radius
                                + "v 1"
                                + "a" + radius + "," + radius + " 0 0 1 " + -radius + "," + radius
                                + "h" + ((radius - width) / 3)
                                + "L " + (x + ((radius - width) / 3 * -1) + (((radius - width) / 3 * -1) / 2)) + " " + (y + 28) + " L" + (x + ((radius - width) / 3 * -1)) + " " + (y + 25)
                                + "h" + ((radius - width) / 3)
                                + "a" + radius + "," + radius + " 0 0 1 -" + radius + ",-" + radius
                                + "v -1"
                                + "a" + radius + "," + radius + " 0 0 1 " + radius + "," + -radius
                                + "z";
                        }

                        function addBars() {
                            var drawedBars =_.filter(normalicedData, function (item) {
                                return item.total !== 0;
                            });
                            var categoryBar = chart.selectAll(".category-volume-evolution-bar")
                                .data(drawedBars)
                                .enter().append("g")
                                .attr("class", function (d) {
                                    return "category-volume-evolution-bar " + d.category;
                                })
                                .attr("transform", function(d) {
                                    return "translate(" + xScale(d.category) + ",0)";
                                });

                            categoryBar.selectAll("path.vertical-bars-multi-dim")
                                .data(function(d) {
                                    return _.map(d.volumeMentions, function (volumeMention) {
                                        return _.assign(volumeMention, {category: d.category})
                                    })
                                })
                                .enter()
                                .append("path")
                                .attr("class", function (d) {
                                    return "vertical-bars-multi-dim " + d.category;
                                })
                                .attr("d", function(d, i) {
                                    var x = 0;
                                    if (d.y0 != d.y1) {
                                        return rightRoundedRect(x, height, xScale.rangeBand(), 10, 0);
                                    }
                                })
                                .attr("fill", function(d) {
                                    return color(d.name);
                                })
                                .style('fill-opacity', ".1")
                                .attr('cursor','pointer')
                                .on("mouseover",function (d, i) {
                                    d3.select(this).style('fill-opacity', .2);
                                })
                                .on("mouseout", function(d, i) {
                                    d3.selectAll('path.vertical-bars-multi-dim.' + d.category).style('fill-opacity', function () {
                                        return 1;
                                    });
                                    verticalBarTooltip.classed("hidden", true)
                                })
                                .on("mousemove", function(d, i) {
                                    updateToolTip(d);
                                })
                                .transition()
                                .duration(750)
                                .style('fill-opacity', "1")
                                .attr("d", function(d, i) {
                                    var x = 0;
                                    return rightRoundedRect(x, yScale(d.y1), xScale.rangeBand(), yScale(d.y0) - yScale(d.y1), 0);
                                });
                            scope.barsAdded = true;
                        }

                        function rightRoundedRect(x, y, width, height, radius) {
                            return "M" + x + "," + y
                                + "h" + (width - radius)
                                + "a" + radius + "," + radius + " 0 0 1 " + radius + "," + radius
                                + "v" + (height - 2 * radius)
                                + "a" + radius + "," + radius + " 0 0 1 " + -radius + "," + radius
                                + "h" + (radius - width)
                                + "z";
                        }

                        function addLineTicks() {
                            var numberOfTicks = 4;
                            chart.selectAll("line.grid")
                                .data(yScale
                                    .ticks(4))
                                .enter().append("line")
                                .attr("class", "grid")
                                .attr("x1", 0)
                                .attr("x2", width)
                                .attr("y1", yScale)
                                .attr("y2", yScale);
                        }

                        function addXScale () {
                            chart.append("g")
                                .attr("class", "x-axis")
                                .attr("transform", "translate(0," + height + ")")
                                .call(xAxis)
                                .selectAll("text")
                                .style("text-anchor", "end")
                                .attr("transform", "rotate(-30)" );
                        }

                        function addYScale () {
                            chart.append("g")
                                .attr("class", "y-axis")
                                .call(yAxis);
                        }

                        function updateToolTip(d) {
                            d3.event.preventDefault();
                            var mouse = d3.mouse(svg.node()).map( function(d) { return parseInt(d); } );
                            var mentions = _.result(_.find(_.result(_.find(scope.items, { 'category': d.category}), 'data'), { 'brand': d.name}), 'volume');
                            //FIXME: refactor to directive
                            verticalBarTooltip
                                .classed("hidden", false)
                                .attr("style", "left:"+(mouse[0]- 70)+"px;top:"+ (mouse[1]) +"px")
                                .html(
                                "<div>" +
                                    "<div style='position: relative; padding: 10px; border-bottom: 1px solid #e9e9e9; font-size: 10px;'>" +
                                        "<div style='height: 12px; width: 12px; border-radius: 6px; background-color:" + _.result(_.find(scope.brands, { 'brand': d.name}), 'color') + "'></div>" +
                                        "<span style='position: absolute; top: 10px; left: 28px;'>" + d.name + "</span>" +
                                    "</div>" +
                                    "<p style='padding: 10px; width: 90px; height: 113px; margin: 0px; font-size: 12px;'> " +
                                        "<strong style='font-size: 14px;'>" + $filter('number')(mentions) + "</strong> " +
                                        "menciones " +
                                        "<strong style='font-size: 14px;'>" + $filter('number')((100 / _.result(_.find(normalicedData, { 'category': d.category}), 'total') * mentions).toFixed(2)) + "%</strong> " +
                                        "categoría " + d.category +

                                    "</p>" +
                                "</div>"
                            );
                        }
                    }, 0);
                }
            }
        }
    };
}
exports.dtVerticalBarsMultiDimensions = ['$window', '$timeout', '$filter', dtVerticalBarsMultiDimensions];