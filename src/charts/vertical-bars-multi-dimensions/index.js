'use strict';

var verticalBarsMultiDimensions = module.exports = angular.module('dtcharts.charts.verticalBarsMultiDimensions', []);

require('./dtVerticalBarsMultiDimensions').inject(verticalBarsMultiDimensions);