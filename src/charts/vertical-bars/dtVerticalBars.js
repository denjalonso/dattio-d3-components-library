'use strict';

var d3 = require('d3');
var _ = require('lodash');

exports.inject = function(verticalBars) {
    verticalBars.directive('dtVerticalBars', exports.dtVerticalBars);
    return exports.dtPercentHorizontalBars;
};

var dtVerticalBars = function ($window, $timeout, $filter) {
    function calculatePercent(number, percent) {
        return number * percent / 100;
    }
    function calculateBadgeWidth(proportionalBadgeWidth) {
        var maxBadgeWidth = 37, minBadgeWidth = 23;
        if (proportionalBadgeWidth < maxBadgeWidth && proportionalBadgeWidth > minBadgeWidth) {
            return proportionalBadgeWidth;
        } else if (proportionalBadgeWidth < minBadgeWidth) {
            return minBadgeWidth;
        } else if (proportionalBadgeWidth > maxBadgeWidth) {
            return maxBadgeWidth
        }
    }
    return {
        restrict: 'EA',
        replace: true,
        templateUrl: 'vertical-bars/dt-vertical-bars.html',
        scope: {
            items: '='
        },
        link: {
            pre: function (scope, ele, attrs) {
                //http://www.ng-newsletter.com/posts/d3-on-angular.html
                var renderTimeout;

                //TODO: to constant
                var defaultBarWidth = 52;
                var defaultBarMargin = 32;
                var defaultBarPadding = 0;

                var linesLeftPaddingPercent = 7.78;
                var linesBottomPaddingPercent = 13;
                var linesTopPaddingPercent = 10.17;
                var barsLeftPaddingPercent = 9.22;
                var precisionPixel = 1;
                var barsSectionWidthPercent = 78.22;
                var idealRadiusPorportion = 10 / 117;
                var idealBadgeWidthProportion = 37 / 74;

                var margin = parseInt(attrs.margin) || defaultBarMargin,
                    barWidth = parseInt(attrs.barHeight) || defaultBarWidth,
                    barPadding = parseInt(attrs.barPadding) || defaultBarPadding;

                var widthPixels = d3.select(ele[0].getElementsByClassName('svg-container-vertical-bars')[0]).node().offsetWidth;
                var heightPixels = d3.select(ele[0].getElementsByClassName('svg-container-vertical-bars')[0]).node().offsetHeight;
                var chartMargin = {top: 0, right: 15, bottom: 0, left: 38};
                var percent = d3.format('%');

                scope.barsAdded = true;

                var color = d3.scale.ordinal()
                    .range(["#81d8eb", "#63a5cb", "#4571ab"])
                    .domain(["posMsg", "neuMsg", "negMsg"]);

                var svg = d3.select(ele.find("svg")[0]);

                var verticalChart = svg.append("g")
                    .attr("class", "vertical-chart");

                var chart = verticalChart.append("g")
                    .attr("class", "chart")
                    .attr("transform", "translate(" + chartMargin.left + "," + chartMargin.top + ")");

                var verticalBarTooltip = d3.select('.svg-container-vertical-bars').append("div")
                    .attr("class", "vertical-bar-tooltip")
                    .style('display', "none");

                $window.onresize = function () {
                    scope.$apply();
                };

                scope.$watch(function () {
                    return angular.element($window)[0].innerWidth || angular.element($window)[0].innerHeight;
                }, function () {
                    $timeout(function () {
                        scope.render();
                    }, 750);    //Same as reset transition
                });

                scope.$watch('items', function(newVals, oldVals) {
                    if (newVals !== oldVals) {
                        $timeout(function () {
                            scope.render();
                        }, 750);    //Same as reset transition
                    }
                }, false);

                scope.render = function () {
                    verticalChart.selectAll("*").remove();
                    svg.selectAll(".category-badge").remove();
                    svg.selectAll(".badge-text").remove();

                    if (renderTimeout) clearTimeout(renderTimeout);

                    renderTimeout = $timeout(function () {

                        widthPixels = d3.select(ele[0].getElementsByClassName('svg-container-vertical-bars')[0]).node().offsetWidth;
                        heightPixels = d3.select(ele[0].getElementsByClassName('svg-container-vertical-bars')[0]).node().offsetHeight;
                        chartMargin = {
                            top: calculatePercent(heightPixels, linesTopPaddingPercent) + precisionPixel,
                            right: 0,
                            bottom: calculatePercent(heightPixels, linesBottomPaddingPercent) + precisionPixel,
                            left: calculatePercent(widthPixels, linesLeftPaddingPercent) + precisionPixel
                        };
                        var width = widthPixels - chartMargin.left - chartMargin.right,
                            height = heightPixels - chartMargin.top - chartMargin.bottom;

                        var barsSectionWidth = calculatePercent(width, barsSectionWidthPercent);
                        var numberOfBars = scope.items.length;
                        var calculatedBarWidth = ((barsSectionWidth / numberOfBars).toFixed(2) || barWidth) - barPadding;
                        if (defaultBarWidth < calculatedBarWidth) {
                            scope.calculatedBarWidth = defaultBarWidth;
                        }

                        var barsSectionLeftPadding = calculatePercent(widthPixels, barsLeftPaddingPercent);
                        scope.barPadding = barPadding;

                        var xScale = d3.scale.ordinal()
                            .rangeRoundBands([0, width], .3, 0.5);

                        var yScale = d3.scale.linear()
                            .rangeRound([height, 0]);

                        var xAxis = d3.svg.axis()
                            .scale(xScale)
                            .orient("bottom")
                            .tickFormat(function (d) {
                                return $filter('capitalize')(d)
                            });

                        var yAxis = d3.svg.axis()
                            .scale(yScale)
                            .orient("left")
                            .ticks(4)
                            .tickFormat(d3.format(".0%"));

                        scope.proportionalRadius = idealRadiusPorportion * xScale.rangeBand();

                        scope.items.forEach(function(d) {
                            var y0 = 0;
                            d.mentions = color.domain().map(function(name) {
                                return {
                                    name: name,
                                    y0: y0,
                                    y1: y0 += +d[name]};
                            });
                            d.mentions.forEach(function(d) {
                                d.y0 /= y0;
                                d.y1 /= y0;
                            });
                        });

                        scope.items.sort(function(a, b) {
                            return b.mentions[0].y1 - a.mentions[0].y1;
                        });


                        chart = verticalChart.append("g")
                            .attr("class", "chart")
                            .attr("transform", "translate(" + chartMargin.left + "," + chartMargin.top + ")");

                        addXScale();
                        addYScale();
                        addLineTicks();
                        addBars();
                        addBadges();
                        addBadgesText();

                        function addBadgesText() {
                            var drawedBadgesText =_.filter(scope.items, function (item) {
                                return _.some([item.posMsg, item.negMsg, item.neuMsg], function (n) {
                                    return n !== 0;
                                });
                            });
                            svg.selectAll("text.badge-text")
                                .data(drawedBadgesText)
                                .enter()
                                .append("text")
                                .attr("class", "badge-text")
                                .attr("text-anchor", "middle")
                                .text(function (d) {
                                    return $filter('number')(((d.posMsg + d.negMsg + d.neuMsg) / 1000).toFixed(2)) + "K";
                                })
                                .attr("x", function(d) {
                                    var proportionalBadgeWidth = idealBadgeWidthProportion * xScale.rangeBand();
                                    var badgeWidth = calculateBadgeWidth(proportionalBadgeWidth);
                                    var diference = 37 - badgeWidth;
                                    return (xScale.rangeBand()/2) + chartMargin.left - (12.75 - (diference / 2)) + xScale(d.category) + ((d.total / 1000) > 99 ? 10 - (diference / 2) : 14 - (diference / 2));
                                })
                                .attr("y", 16)
                                .attr("fill", "white");
                        }

                        function addBadges() {
                            var drawedBadges =_.filter(scope.items, function (item) {
                                return _.some([item.posMsg, item.negMsg, item.neuMsg], function (n) {
                                    return n !== 0;
                                });
                            });
                            svg.selectAll("path.category-badge")
                                .data(drawedBadges)
                                .enter()
                                .append("path")
                                .attr("class", "category-badge")
                                .attr("d", function(d, i) {
                                    var proportionalBadgeWidth = idealBadgeWidthProportion * xScale.rangeBand();
                                    var radius = 12;
                                    var badgeWidth = calculateBadgeWidth(proportionalBadgeWidth);
                                    var diference = 37 - badgeWidth;
                                    return badge((xScale.rangeBand()/2) + chartMargin.left - (12.75 - (diference / 2)) + xScale(d.category), 0, badgeWidth, radius);
                                })
                                .attr("fill", "black");
                        }

                        function badge (x, y , width, radius) {
                            return "M" + x + "," + y
                                + "h" + (width - radius)
                                + "a" + radius + "," + radius + " 0 0 1 " + radius + "," + radius
                                + "v 1"
                                + "a" + radius + "," + radius + " 0 0 1 " + -radius + "," + radius
                                + "h" + ((radius - width) / 3)
                                + "L " + (x + ((radius - width) / 3 * -1) + (((radius - width) / 3 * -1) / 2)) + " " + (y + 28) + " L" + (x + ((radius - width) / 3 * -1)) + " " + (y + 25)
                                + "h" + ((radius - width) / 3)
                                + "a" + radius + "," + radius + " 0 0 1 -" + radius + ",-" + radius
                                + "v -1"
                                + "a" + radius + "," + radius + " 0 0 1 " + radius + "," + -radius
                                + "z";
                        }

                        function addBars() {
                            var categoryBar = chart.selectAll(".category-bar")
                                .data(scope.items)
                                .enter().append("g")
                                .attr("class", function (d) {
                                    return "category-bar " + d.category;
                                })
                                .attr("transform", function(d) { return "translate(" + xScale(d.category) + ",0)"; });

                            categoryBar.selectAll("path.vertical-bars")
                                .data(function(d) {
                                    return _.map(d.mentions, function (mention) { return _.assign(mention, {category: d.category})})
                                })
                                .enter()
                                .append("path")
                                .attr("class", function (d) {
                                    return "vertical-bars " + d.category;
                                })
                                .attr("d", function(d, i) {
                                    if (d.y0 != d.y1) {
                                        return rightRoundedRect(0, yScale(0), xScale.rangeBand(), 10, 0);
                                    }
                                })
                                .attr("fill", function(d) {
                                    return color(d.name);
                                })
                                .style('fill-opacity', ".1")
                                .attr('cursor','pointer')
                                .on("mouseover",function (d, i) {
                                    d3.select(this).style('fill-opacity', .2);
                                })
                                .on("mouseout", function(d, i) {
                                    d3.selectAll('path.vertical-bars').style('fill-opacity', function () {
                                        return 1;
                                    });
                                    verticalBarTooltip.classed("hidden", true)
                                })
                                .on("mousemove", function(d, i) {
                                    updateToolTip(d);
                                })
                                .transition()
                                .duration(750)
                                .style('fill-opacity', "1")
                                .attr("d", function(d, i) {
                                    var x = 0, radius = 0;
                                    if (d.y0 != d.y1) {
                                        if (d.y1 === 1 && d.y0 !== 0) {
                                            return rightRoundedRect(x, yScale(0), xScale.rangeBand(), yScale(d.y1) - yScale(d.y0), radius);
                                        } else {
                                            return rightRoundedRect(x, yScale(0) - yScale(d.y0), xScale.rangeBand(), yScale(d.y0) - yScale(d.y1), radius);
                                        }
                                    }
                                });
                            scope.barsAdded = true;
                        }

                        function rightRoundedRect(x, y, width, height, radius) {
                            return "M" + x + "," + y
                                + "h" + (width - radius)
                                + "a" + radius + "," + radius + " 0 0 1 " + radius + "," + radius
                                + "v" + (height - 2 * radius)
                                + "a" + radius + "," + radius + " 0 0 1 " + -radius + "," + radius
                                + "h" + (radius - width)
                                + "z";
                        }

                        function addLineTicks() {
                            var numberOfTicks = 4;
                            chart.selectAll("line.grid")
                                .data(yScale
                                    .ticks(4))
                                .enter().append("line")
                                .attr("class", "grid")
                                .attr("x1", 0)
                                .attr("x2", function (d, i) {
                                    return i === 0 || i === numberOfTicks + 1 ? calculatePercent(width, 4.88) : width;
                                })
                                .attr("y1", yScale)
                                .attr("y2", yScale);
                        }

                        function addXScale () {
                            xScale.domain(scope.items.map(function(d) {
                                return d.category;
                            }));
                            chart.append("g")
                                .attr("class", "x-axis")
                                .attr("transform", "translate(0," + height + ")")
                                .call(xAxis)
                                .selectAll("text")
                                .style("text-anchor", "end")
                                .attr("transform", "rotate(-30)" );
                        }

                        function addYScale () {
                            chart.append("g")
                                .attr("class", "y-axis")
                                .call(yAxis);
                        }

                        function updateToolTip(d) {
                            d3.event.preventDefault();
                            var mouse = d3.mouse(svg.node()).map( function(d) { return parseInt(d); } );
                            if (d.name === 'negMsg') {
                                scope.section = 'negativo';
                            } else if (d.name === 'neuMsg') {
                                scope.section = 'neutro';
                            } else {
                                scope.section = 'positivo';
                            }
                            //FIXME: refactor to directive
                            verticalBarTooltip
                                .classed("hidden", false)
                                .attr("style", "left:"+(mouse[0]- 60)+"px;top:"+ (mouse[1]) +"px")
                                .html("<p style='width: 60px; height: 60px; margin: 0px; font-size: 10px;'> <strong style='font-size: 14px;'>" + $filter('number')(((d.y1 - d.y0) * 100).toFixed(2)) + "%</strong> del total de la categoría es " + scope.section + "</p>");        //Get CCAA by norm_code
                        }
                    }, 0);
                }
            }
        }
    };
}
exports.dtVerticalBars = ['$window', '$timeout', '$filter', dtVerticalBars];