'use strict';

var verticalBars = module.exports = angular.module('dtcharts.charts.verticalBars', []);

require('./dtVerticalBars').inject(verticalBars);