var angular = require('angular');

exports.inject = function(filters) {
    filters.filter('capitalize', exports.capitalize);
    return exports.capitalize;
};

module.capitalize = function () {
    return function(token) {
        return token.charAt(0).toUpperCase() + token.slice(1);
    }
}