'use strict';

var filters = module.exports = angular.module('dtcharts.filters', []);

require('./CapitalizeFilter').inject(filters);