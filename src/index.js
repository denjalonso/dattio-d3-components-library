"use strict";

var angular = require('angular');

require('./templates');

require('./filters');

angular.module('dtcharts', [
    'templates',
    require('./charts').name,
    require('./filters').name
])
    .constant('version', require('../package.json').version);
